<?php get_header(); ?>


 
    <div class="section section__404">
        <div class="container">

            <div class="page404__block">
                <h1 class="page404__title">
                    404
                </h1>  
                <div class="page404__title2">
                    Страница не найдена 
                </div>   
                <div class="page404__btn">
                    <a href="/" class="page404__linck">На главную</a>
                </div>
                
            </div>

        </div>
    </div>
    
    <style>
        .section.section__404 {
            padding: 70px 0;
        }
    </style>

<?php get_footer(); ?>