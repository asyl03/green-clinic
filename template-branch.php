<?php
/*
    Template Name: Филиал
    Template Post Type: page
*/
?>


<?php get_header(); ?>

    <div class="section__banner">
        <div class="section__banner--img">
            <img src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/banner-img-branch.jpg" alt="Многопрофильный медицинский центр-Green Clinic">
        </div>
        <div class="container">
            <div class="section__banner--info">
                <div class="section__banner--title">
                    Филиал
                </div>
                <div class="section__banner--text">
                    Это  Многопрофильный медицинский центр, предоставляющий  доступную высококачественную медицинскую помощь населению, и  возможность пройти полное  обследование у врачей различного профиля
                </div>
                <div class="section__banner--linck">
                    <a href="<?php echo get_permalink(7); ?>" class="button__shadow">Узнать больше</a>
                </div>
            </div>
        </div>
    </div>


    <div class="section__branch">
        <div class="container">
            <div class="branch__text">
                <?php the_content(); ?>
            </div>
        </div>
    </div>


    <div class="section__services section__margin">
        <div class="container">

            <div class="section__title">
                <div class="section__title--main">
                    Услуги
                </div>
                <div class="section__title--desc">
                    Высокопрофильные специалисты не оставят вас в трудную минуту
                </div>
            </div>


            <div class="services__filter">
                <div class="services__filter--button">
                    <div class="services__filter__button--item services__filter__button--active" data-id="16">
                        Поликлиника
                    </div>
                    <div class="services__filter__button--item" data-id="17">
                        Стационар
                    </div>
                    <div class="services__filter__button--item" data-id="18">
                        Отделение лучевой диагностики 
                    </div>
                </div>


                <div class="services__filter--slider">
                    
                    <div class="services__filter--content swiper-wrapper">

                        <?php foreach(get_field('poliklinika') as $item_u){ ?>

                            <div class="swiper-slide services__filter--item services__filter__item--16 services__filter__item--active">
                                <div class="services__filter--itemblock">
                                    <div class="services__filter__item--img">
                                        <?php echo get_the_post_thumbnail($item_u, 'full'); ?>
                                    </div>
                                    <div class="services__filter__item--title">
                                        <?php echo get_the_title( $item_u ); ?>
                                    </div>
                                    <a href="<?php echo get_permalink( $item_u ); ?>" class="services__filter__item--linck">Подробнее</a>
                                </div>
                            </div>

                        <?php } ?>
                        <?php foreach(get_field('staczionar') as $item_u){ ?>

                            <div class="swiper-slide services__filter--item services__filter__item--17">
                                <div class="services__filter--itemblock">
                                    <div class="services__filter__item--img">
                                        <?php echo get_the_post_thumbnail($item_u, 'full'); ?>
                                    </div>
                                    <div class="services__filter__item--title">
                                        <?php echo get_the_title( $item_u ); ?>
                                    </div>
                                    <a href="<?php echo get_permalink( $item_u ); ?>" class="services__filter__item--linck">Подробнее</a>
                                </div>
                            </div>

                        <?php } ?>
                        <?php foreach(get_field('otdelenie_luchevoj_diagnostiki') as $item_u){ ?>

                            <div class="swiper-slide services__filter--item services__filter__item--18">
                                <div class="services__filter--itemblock">
                                    <div class="services__filter__item--img">
                                        <?php echo get_the_post_thumbnail($item_u, 'full'); ?>
                                    </div>
                                    <div class="services__filter__item--title">
                                        <?php echo get_the_title( $item_u ); ?>
                                    </div>
                                    <a href="<?php echo get_permalink( $item_u ); ?>" class="services__filter__item--linck">Подробнее</a>
                                </div>
                            </div>

                        <?php } ?>

                    <?php //while ( $control16->have_posts() ) { $control16->the_post(); ?>
                            
                        <?php //get_template_part( 'components/component', 'service16' ); ?>
                       
                    <?php //} ?>

                    <?php //while ( $control17->have_posts() ) { $control17->the_post(); ?>
                            
                        <?php //get_template_part( 'components/component', 'service17' ); ?>
                        
                    <?php //} ?>

                    <?php //while ( $control18->have_posts() ) { $control18->the_post(); ?>
                            
                        <?php //get_template_part( 'components/component', 'service18' ); ?>
                        
                    <?php //} ?>

                    </div>
                    <div class="services__filter__pagination pagination__block"></div>

                </div>
                
            </div>



        </div>
    </div>


<?php 
  get_template_part( 'components/component', 'form');
?>

<?php 
  get_template_part( 'components/component', 'contact');
?>


<?php get_footer(); ?>