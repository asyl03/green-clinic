<?php 

	register_nav_menus(
	array(
		'mainmenu' => __( 'Главное', 'mainmenu' ),
		'footermenu1' => __( 'Footer 1', 'footermenu1' ),
		'footermenu1-2' => __( 'Footer 1-2', 'footermenu1-2' ),
		'footermenu2' => __( 'Footer 2 ', 'footermenu2' ),
		'footermenu3' => __( 'Footer 3 ', 'footermenu3' ),
		'footermenu4' => __( 'Footer 4 ', 'footermenu4' ),
		'sortdoctors' => __( 'Doctors ', 'sortdoctors' ),
		)
	);
	add_theme_support( 'post-thumbnails' );

	if ( !is_admin() ) wp_deregister_script('jquery');

	/* ---- Хлебные кошки ----*/
	function dimox_breadcrumbs() {

		/* === ОПЦИИ === */
		$text['home']     = 'Главная'; // текст ссылки "Главная"
		switch(wpm_get_language()){
			case 'ru': 
				$text['home']     = 'Главная'; // текст ссылки "Главная"
				break;
			case 'kz': 
				$text['home']     = 'Басты'; // текст ссылки "Главная"
				break;
			case 'en': 
				$text['home']     = 'Home'; // текст ссылки "Главная"
				break;
		}
		$text['category'] = '%s'; // текст для страницы рубрики
		$text['search']   = 'Результаты поиска по запросу "%s"'; // текст для страницы с результатами поиска
		$text['tag']      = 'Записи с тегом "%s"'; // текст для страницы тега
		$text['author']   = 'Статьи автора %s'; // текст для страницы автора
		$text['404']      = 'Ошибка 404'; // текст для страницы 404
		$text['page']     = 'Страница %s'; // текст 'Страница N'
		$text['cpage']    = 'Страница комментариев %s'; // текст 'Страница комментариев N'

		$wrap_before    = '<ul class="bread-crambs">'; // открывающий тег обертки
		$wrap_after     = '</ul><!-- .bread-crambs -->'; // закрывающий тег обертки
		$sep            = ''; // разделитель между "крошками"
		$before         = '<li><span>'; // тег перед текущей "крошкой"
		$after          = '</span></li>'; // тег после текущей "крошки"

		$show_on_home   = 0; // 1 - показывать "хлебные крошки" на главной странице, 0 - не показывать
		$show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
		$show_current   = 1; // 1 - показывать название текущей страницы, 0 - не показывать
		$show_last_sep  = 1; // 1 - показывать последний разделитель, когда название текущей страницы не отображается, 0 - не показывать
		/* === КОНЕЦ ОПЦИЙ === */

		global $post;
		$home_url       = home_url('/');
		$link           = '<li>';
		$link          .= '<a href="%1$s">%2$s</a>';
		$link          .= '';
		$link          .= '</li>';
		$parent_id      = ( $post ) ? $post->post_parent : '';
		$home_link      = sprintf( $link, $home_url, $text['home'], 1 );
		if ( is_home() || is_front_page() ) {
			if ( $show_on_home ) echo $wrap_before . $home_link . $wrap_after;
		} else {
			$position = 0;
			echo $wrap_before;
			if ( $show_home_link ) {
				$position += 1;
				echo $home_link;
			}
			
			if ( is_category() ) {
				
				$parents = get_ancestors( get_query_var('cat'), 'category' );
				foreach ( array_reverse( $parents ) as $cat ) {
					$position += 1;
					if ( $position > 1 ) echo $sep;
					echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
				}
				if ( get_query_var( 'paged' ) ) {
					$position += 1;
					$cat = get_query_var('cat');
					echo $sep . sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
					echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
				} else {
					if ( $show_current ) {
						if ( $position >= 1 ) echo $sep;
						echo $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
					} elseif ( $show_last_sep ) echo $sep;
				}
			} elseif ( is_search() ) {
				if ( get_query_var( 'paged' ) ) {
					$position += 1;
					if ( $show_home_link ) echo $sep;
					echo sprintf( $link, $home_url . '?s=' . get_search_query(), sprintf( $text['search'], get_search_query() ), $position );
					echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
				} else {
					if ( $show_current ) {
						if ( $position >= 1 ) echo $sep;
						echo $before . sprintf( $text['search'], get_search_query() ) . $after;
					} elseif ( $show_last_sep ) echo $sep;
				}
			} elseif ( is_year() ) {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . get_the_time('Y') . $after;
				elseif ( $show_home_link && $show_last_sep ) echo $sep;
			} elseif ( is_month() ) {
				if ( $show_home_link ) echo $sep;
				$position += 1;
				echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position );
				if ( $show_current ) echo $sep . $before . get_the_time('F') . $after;
				elseif ( $show_last_sep ) echo $sep;
			} elseif ( is_day() ) {
				if ( $show_home_link ) echo $sep;
				$position += 1;
				echo sprintf( $link, get_year_link( get_the_time('Y') ), get_the_time('Y'), $position ) . $sep;
				$position += 1;
				echo sprintf( $link, get_month_link( get_the_time('Y'), get_the_time('m') ), get_the_time('F'), $position );
				if ( $show_current ) echo $sep . $before . get_the_time('d') . $after;
				elseif ( $show_last_sep ) echo $sep;
			} elseif ( is_single() && ! is_attachment() ) {
				if ( get_post_type() != 'control_category' ) {
					$terms = wp_get_object_terms($post->ID, 'control_category');   
					if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $taxonomy_slug = $terms[0]->slug;
					$position += 1;
					$post_type = get_post_type_object( get_post_type() );
					$lina = str_replace('%control_category%', $taxonomy_slug, get_post_type_archive_link( $post_type->name ));

					if($post_type->name == 'biznes'){
						switch(wpm_get_language()){
							case 'ru': 
								$control_text = "Бизнес-партнерство";
								break;
							case 'kz': 
								$control_text = "Бизнес серіктестік";
								break;
							case 'en': 
								$control_text = "Business partnership";
								break;
						}
						if ( $position > 1 ){
							echo $sep;
							echo sprintf( $link, $lina, $control_text, $position );
						}
					}elseif($post_type->name == 'konkursy'){
						switch(wpm_get_language()){
							case 'ru': 
								$control_text = "Конкурсы";
								break;
							case 'kz': 
								$control_text = "Конкурстар";
								break;
							case 'en': 
								$control_text = "Bidding";
								break;
						}
						if ( $position > 1 ){
							echo $sep;
							echo sprintf( $link, $lina, $control_text, $position );
						}
					}elseif($post_type->name == 'event'){
						switch(wpm_get_language()){
							case 'ru': 
								$control_text = "Конгрессно-выставочные мероприятия";
								break;
							case 'kz': 
								$control_text = "Конгрестік-көрме іс-шаралары";
								break;
							case 'en': 
								$control_text = "Congress and exhibition events";
								break;
						}
						if ( $position > 1 ){
							echo $sep;
							echo sprintf( $link, $lina, $control_text, $position );
						}
					}elseif($post_type->name == 'media_news'){
						switch(wpm_get_language()){
							case 'ru': 
								$control_text = "Медиа";
								break;
							case 'kz': 
								$control_text = "Медиа";
								break;
							case 'en': 
								$control_text = "Media";
								break;
						}
						if ( $position > 1 ){
							echo $sep;
							echo sprintf( $link, $lina, $control_text, $position );
						}
					}elseif($post_type->name == 'news'){
						switch(wpm_get_language()){
							case 'ru': 
								$control_text = "Новости";
								break;
							case 'kz': 
								$control_text = "Жаңалықтар";
								break;
							case 'en': 
								$control_text = "News";
								break;
						}
						if ( $position > 1 ){
							echo $sep;
							echo sprintf( $link, $lina, $control_text, $position );
						}
					}else{
						if ( $position > 1 ){
							echo $sep;
							echo sprintf( $link, $lina, $post_type->labels->name, $position );
						}
					}
					
					

					
					if ( $show_current ) echo $sep . $before . get_the_title() . $after;
					elseif ( $show_last_sep ) echo $sep;
				} elseif ( get_post_type() != 'post' ) {
					$position += 1;
					$post_type = get_post_type_object( get_post_type() );
					if ( $position > 1 ) echo $sep;
					echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->labels->name, $position );
					if ( $show_current ) echo $sep . $before . get_the_title() . $after;
					elseif ( $show_last_sep ) echo $sep;
				} else {
					$cat = get_the_category(); $catID = $cat[0]->cat_ID;
					$parents = get_ancestors( $catID, 'category' );
					$parents = array_reverse( $parents );
					$parents[] = $catID;
					foreach ( $parents as $cat ) {
						$position += 1;
						if ( $position > 1 ) echo $sep;
						echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
					}
					if ( get_query_var( 'cpage' ) ) {
						$position += 1;
						echo $sep . sprintf( $link, get_permalink(), get_the_title(), $position );
						echo $sep . $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after;
					} else {
						if ( $show_current ) echo $sep . $before . get_the_title() . $after;
						elseif ( $show_last_sep ) echo $sep;
					}
				}
			} elseif ( is_post_type_archive() ) {
				
				$post_type = get_post_type_object( get_post_type() );
				if ( get_query_var( 'paged' ) ) {
					$position += 1;
					if ( $position > 1 ) echo $sep;
					echo sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->label, $position );
					echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
				} else {
					

					if($post_type->name == 'biznes'){
						switch(wpm_get_language()){
							case 'ru': 
								$control_text = "Бизнес-партнерство";
								break;
							case 'kz': 
								$control_text = "Бизнес серіктестік";
								break;
							case 'en': 
								$control_text = "Business partnership";
								break;
						}
						if ( $show_home_link && $show_current ) echo $sep;
						if ( $show_current ) echo $before . $control_text . $after;
						elseif ( $show_home_link && $show_last_sep ) echo $sep;
					}elseif($post_type->name == 'konkursy'){
						switch(wpm_get_language()){
							case 'ru': 
								$control_text = "Конкурсы";
								break;
							case 'kz': 
								$control_text = "Конкурстар";
								break;
							case 'en': 
								$control_text = "Bidding";
								break;
						}
						if ( $show_home_link && $show_current ) echo $sep;
						if ( $show_current ) echo $before . $control_text . $after;
						elseif ( $show_home_link && $show_last_sep ) echo $sep;
					}elseif($post_type->name == 'event'){
						switch(wpm_get_language()){
							case 'ru': 
								$control_text = "Конгрессно-выставочные мероприятия";
								break;
							case 'kz': 
								$control_text = "Конгрестік-көрме іс-шаралары";
								break;
							case 'en': 
								$control_text = "Congress and exhibition events";
								break;
						}
						if ( $show_home_link && $show_current ) echo $sep;
						if ( $show_current ) echo $before . $control_text . $after;
						elseif ( $show_home_link && $show_last_sep ) echo $sep;
					}elseif($post_type->name == 'media_news'){
						switch(wpm_get_language()){
							case 'ru': 
								$control_text = "Медиа";
								break;
							case 'kz': 
								$control_text = "Медиа";
								break;
							case 'en': 
								$control_text = "Media";
								break;
						}
						if ( $show_home_link && $show_current ) echo $sep;
						if ( $show_current ) echo $before . $control_text . $after;
						elseif ( $show_home_link && $show_last_sep ) echo $sep;
					}elseif($post_type->name == 'news'){
						switch(wpm_get_language()){
							case 'ru': 
								$control_text = "Новости";
								break;
							case 'kz': 
								$control_text = "Жаңалықтар";
								break;
							case 'en': 
								$control_text = "News";
								break;
						}
						if ( $show_home_link && $show_current ) echo $sep;
						if ( $show_current ) echo $before . $control_text . $after;
						elseif ( $show_home_link && $show_last_sep ) echo $sep;
					}else{

						if ( $show_home_link && $show_current ) echo $sep;
						if ( $show_current ) echo $before . $post_type->label . $after;
						elseif ( $show_home_link && $show_last_sep ) echo $sep;

					}






				}
			} elseif ( is_attachment() ) {
				
				$parent = get_post( $parent_id );
				$cat = get_the_category( $parent->ID ); $catID = $cat[0]->cat_ID;
				$parents = get_ancestors( $catID, 'category' );
				$parents = array_reverse( $parents );
				$parents[] = $catID;
				foreach ( $parents as $cat ) {
					$position += 1;
					if ( $position > 1 ) echo $sep;
					echo sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ), $position );
				}
				$position += 1;
				echo $sep . sprintf( $link, get_permalink( $parent ), $parent->post_title, $position );
				if ( $show_current ) echo $sep . $before . get_the_title() . $after;
				elseif ( $show_last_sep ) echo $sep;
			} elseif ( is_page() && ! $parent_id ) {
				
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . get_the_title() . $after;
				elseif ( $show_home_link && $show_last_sep ) echo $sep;
			} elseif ( is_page() && $parent_id ) {
				$parents = get_post_ancestors( get_the_ID() );
				foreach ( array_reverse( $parents ) as $pageID ) {
					$position += 1;
					if ( $position > 1 ) echo $sep;
					echo sprintf( $link, get_page_link( $pageID ), get_the_title( $pageID ), $position );
				}
				if ( $show_current ) echo $sep . $before . get_the_title() . $after;
				elseif ( $show_last_sep ) echo $sep;
			} elseif ( is_tag() ) {
				if ( get_query_var( 'paged' ) ) {
					$position += 1;
					$tagID = get_query_var( 'tag_id' );
					echo $sep . sprintf( $link, get_tag_link( $tagID ), single_tag_title( '', false ), $position );
					echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
				} else {
					if ( $show_home_link && $show_current ) echo $sep;
					if ( $show_current ) echo $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;
					elseif ( $show_home_link && $show_last_sep ) echo $sep;
				}
			} elseif ( is_author() ) {
				$author = get_userdata( get_query_var( 'author' ) );
				if ( get_query_var( 'paged' ) ) {
					$position += 1;
					echo $sep . sprintf( $link, get_author_posts_url( $author->ID ), sprintf( $text['author'], $author->display_name ), $position );
					echo $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
				} else {
					if ( $show_home_link && $show_current ) echo $sep;
					if ( $show_current ) echo $before . sprintf( $text['author'], $author->display_name ) . $after;
					elseif ( $show_home_link && $show_last_sep ) echo $sep;
				}
			} elseif ( is_404() ) {
				if ( $show_home_link && $show_current ) echo $sep;
				if ( $show_current ) echo $before . $text['404'] . $after;
				elseif ( $show_last_sep ) echo $sep;
			} elseif ( has_post_format() && ! is_singular() ) {
				if ( $show_home_link && $show_current ) echo $sep;
				echo get_post_format_string( get_post_format() );
			} elseif(get_post_type() == 'control'){

				switch(wpm_get_language()){
					case 'ru': 
						$control_text = "Корпоративное управление";
						break;
					case 'kz': 
						$control_text = "Корпоративтік басқару";
						break;
					case 'en': 
						$control_text = "Corporate governance";
						break;
				}

				$position += 1;
				echo sprintf( $link, get_term_link(16, 'control_category'), $control_text, $position );

			}elseif(is_archive()){ //get_post_type() == 'doctor'

				switch(wpm_get_language()){
					case 'ru': 
						$control_text = "Врачи";
						break;
					case 'kz': 
						$control_text = "Врачи";
						break;
					case 'en': 
						$control_text = "Врачи";
						break;
				}

				$position += 1;
				echo sprintf( $link, get_post_type_archive_link('doctor'), $control_text, $position );

				if ( $show_current ) {
					if ( $position >= 1 ) echo $sep;
					echo $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;
				} 
				

			}else{
				
			}
			
			echo $wrap_after;
		}
	}


	/* ---- Вывод языков ----  */
	function my_site_custom_languages_selector_template () {
		if (function_exists('wpm_get_languages')) {
		$languages = wpm_get_languages();
		$current = wpm_get_language();
		$out = '';

		$out .= '<div class="header__lang--click">';
				$out .= '<a>'. $languages[$current]['name'] .'</a>';
		$out .= '</div>';
		$out .= '<div class="header__lang--menu">';
				$out .= '<ul>';
						foreach ($languages as $code => $language) {
								$toggle_url = esc_url(wpm_translate_current_url($code));
								$css_classes = ' ';
								if ($code === $current) {
								$css_classes .= 'header__lang--active';
								}
								$out .= '<li class="header__lang--item ' . $css_classes . '"><a href="' . $toggle_url . '" data-lang="' . esc_attr($code) . '">';
								$out .= $language['name'];
								$out .= '</a></li>';
						}
				$out .= '</ul>';
		$out .= '</div>';

		
		$out .= '';
		return $out;
		}
	}




	add_action( 'init', 'news' ); // Использовать функцию только внутри хука init
	function news() {
		$labels = array(
			'name' => 'Новости',
			'singular_name' => 'Новость', // админ панель Добавить->Функцию
			'add_new' => 'Добавить новость',
			'add_new_item' => 'Добавить новую новость', // заголовок тега <title>
			'edit_item' => 'Редактировать новость',
			'new_item' => 'Новая новость',
			'all_items' => 'Все новости',
			'view_item' => 'Просмотр новостей на сайте',
			'search_items' => 'Искать новость',
			'not_found' =>  'Новость не найдена.',
			'not_found_in_trash' => 'В корзине нет новостей.',
			'parent_item_colon' => 'Родительская новость',
			'menu_name' => 'Новости' // ссылка в меню в админке
		);
		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true, // показывать интерфейс в админке
			'has_archive' => true, 
			'menu_icon' => 'dashicons-plus-alt',
			'menu_position' => 20, // порядок в меню
			'hierarchical' => false,
			'rewrite' => array(
					'slug' => 'news',
			),
			'supports' => array('title','editor','thumbnail','page-attributes','custom-fields')
		);
		register_post_type('news', $args);
	}


	add_action( 'init', 'departments' ); // Использовать функцию только внутри хука init
	function departments() {
		$labels = array(
			'name' => 'Отделения',
			'singular_name' => 'Отделение', // админ панель Добавить->Функцию
			'add_new' => 'Добавить отделение',
			'add_new_item' => 'Добавить новую отделение', // заголовок тега <title>
			'edit_item' => 'Редактировать отделение',
			'new_item' => 'Новая отделение',
			'all_items' => 'Все отделения',
			'view_item' => 'Просмотр отделение на сайте',
			'search_items' => 'Искать отделение',
			'not_found' =>  'Отделение не найдена.',
			'not_found_in_trash' => 'В корзине нет отделение.',
			'parent_item_colon' => 'Родительская отделение',
			'menu_name' => 'Отделения' // ссылка в меню в админке
		);
		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true, // показывать интерфейс в админке
			'has_archive' => true, 
			'menu_icon' => 'dashicons-plus-alt',
			'menu_position' => 20, // порядок в меню
			'hierarchical' => false,
			'rewrite' => array(
					'slug' => 'departments',
			),
			'supports' => array('title','editor','thumbnail','page-attributes','custom-fields')
		);
		register_post_type('departments', $args);
	}



/*
	add_filter('post_link', 'control_permalink', 10, 3);
	add_filter('post_type_link', 'control_permalink', 10, 3);
	function control_permalink($permalink, $post_id, $leavename) {
		if (strpos($permalink, '%control_category%') === FALSE) return $permalink;
		// Get post
		$post = get_post($post_id);
		if (!$post) return $permalink;
		// Get taxonomy terms
		$terms = wp_get_object_terms($post->ID, 'control_category');   

		if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $taxonomy_slug = $terms[0]->slug;
		else $taxonomy_slug = 'not-rated';
		return str_replace('%control_category%', $taxonomy_slug, $permalink);
	}
*/


	add_action( 'init', 'control' ); // Использовать функцию только внутри хука init
	function control() {

		
        $labels = array(
            'name' => 'Услуги',
            'singular_name' => 'Услуги', // админ панель Добавить->Функцию
            'add_new' => 'Добавить услугу',
            'add_new_item' => 'Добавить новую услугу', // заголовок тега <title>
            'edit_item' => 'Редактировать услугу',
            'new_item' => 'Новый услуга',
            'all_items' => 'Все услуги',
            'view_item' => 'Просмотр услугу на сайте',
            'search_items' => 'Искать услугу',
            'not_found' =>  'Услуга не найдена.',
            'not_found_in_trash' => 'В корзине нет услуги.',
            'parent_item_colon' => 'Родительский услуга',
            'menu_name' => 'Услуги' // ссылка в меню в админке
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
		    // 'publicly_queryable' => true,
            'show_ui' => true, // показывать интерфейс в админке
            'has_archive' => true, 
            'menu_icon' => 'dashicons-plus-alt',
            'menu_position' => 21, // порядок в меню
            'hierarchical' => false,
			// 'rewrite' => array('slug' => 'control/%control_category%', 'with_front' => false),
			'rewrite' => array('slug' => 'control'),
			'taxonomies'  => array( 'control_category' ),
            'supports' => array('title','editor','thumbnail','page-attributes','custom-fields')
        );
        register_post_type('control', $args);

		register_taxonomy( 'control_category', [ 'control' ], [
			'label'                 => 'Раздел услуги', // определяется параметром $labels->name
			'labels'                => array(
				'name'              => 'Разделы услуги',
				'singular_name'     => 'Раздел услуги',
				'search_items'      => 'Искать Раздел услуги',
				'all_items'         => 'Все Разделы услуги',
				'parent_item'       => 'Родит. раздел услуги',
				'parent_item_colon' => 'Родит. раздел услуги:',
				'edit_item'         => 'Ред. Раздел услуги',
				'update_item'       => 'Обновить Раздел услуги',
				'add_new_item'      => 'Добавить Раздел вопроса',
				'new_item_name'     => 'Новый Раздел услуги',
				'menu_name'         => 'Раздел услуги',
			),
			'description'           => 'Рубрики для раздела услуги', // описание таксономии
			'public'                => true,
			'show_in_nav_menus'     => true, // равен аргументу public
			'show_ui'               => true, // равен аргументу public
			'show_tagcloud'         => true, // равен аргументу show_ui
			'hierarchical'          => true,
			'show_in_nav_menus '    => true,
			// 'rewrite'               => array('slug'=>'control_category', 'hierarchical'=>false, 'with_front'=>false, 'feed'=>false ),
			'rewrite'               => array('slug'=>'control', 'hierarchical'=>false, 'with_front'=>false, 'feed'=>false ),
			'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		] );

	}



/*
	add_filter('post_link', 'doctor_permalink', 24, 3);
	add_filter('post_type_link', 'doctor_permalink', 24, 3);
	function doctor_permalink($permalink, $post_id, $leavename) {
			if (strpos($permalink, '%doctor_category%') === FALSE) return $permalink;
			// Get post
			$post = get_post($post_id);
			if (!$post) return $permalink;
			// Get taxonomy terms
			$terms = wp_get_object_terms($post->ID, 'doctor_category');   

			if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $taxonomy_slug = $terms[0]->slug;
			else $taxonomy_slug = 'not-rated';
		return str_replace('%doctor_category%', $taxonomy_slug, $permalink);
	}
*/
	add_action( 'init', 'doctor' ); // Использовать функцию только внутри хука init
	function doctor() {

        $labels = array(
            'name' => 'Врачи',
            'singular_name' => 'Доктор', // админ панель Добавить->Функцию
            'add_new' => 'Добавить доктора',
            'add_new_item' => 'Добавить новую доктора', // заголовок тега <title>
            'edit_item' => 'Редактировать доктора',
            'new_item' => 'Новый доктор',
            'all_items' => 'Все Доктор',
            'view_item' => 'Просмотр доктора на сайте',
            'search_items' => 'Искать доктора',
            'not_found' =>  'Доктор не найдена.',
            'not_found_in_trash' => 'В корзине нет Доктор.',
            'parent_item_colon' => 'Родительский доктор',
            'menu_name' => 'Врачи' // ссылка в меню в админке
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
		    'publicly_queryable' => true,
            'show_ui' => true, // показывать интерфейс в админке
            'has_archive' => true, 
            'menu_icon' => 'dashicons-plus-alt',
            'menu_position' => 21, // порядок в меню
            'hierarchical' => false,
			// 'rewrite' => array('slug' => 'doctor/%doctor_category%', 'with_front' => false),
			'rewrite' => array('slug' => 'doctor'),
			'taxonomies'  => array( 'doctor_category' ),
            'supports' => array('title','editor','thumbnail','page-attributes','custom-fields')
        );
        register_post_type('doctor', $args);

		register_taxonomy( 'doctor_category', [ 'doctor' ], [
			'label'                 => 'Раздел доктора', // определяется параметром $labels->name
			'labels'                => array(
				'name'              => 'Разделы доктора',
				'singular_name'     => 'Раздел доктора',
				'search_items'      => 'Искать Раздел доктора',
				'all_items'         => 'Все Разделы доктора',
				'parent_item'       => 'Родит. раздел доктора',
				'parent_item_colon' => 'Родит. раздел доктора:',
				'edit_item'         => 'Ред. Раздел доктора',
				'update_item'       => 'Обновить Раздел доктора',
				'add_new_item'      => 'Добавить Раздел вопроса',
				'new_item_name'     => 'Новый Раздел доктора',
				'menu_name'         => 'Раздел доктора',
			),
			'description'           => 'Рубрики для раздела доктора', // описание таксономии
			'public'                => true,
			'show_in_nav_menus'     => true, // равен аргументу public
			'show_ui'               => true, // равен аргументу public
			'show_tagcloud'         => true, // равен аргументу show_ui
			'hierarchical'          => true,
			'show_in_nav_menus '    => true,
			'rewrite'               => array('slug'=>'doctor_category', 'hierarchical'=>false, 'with_front'=>false, 'feed'=>false ),
			'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		] );
	}



 

	/**
	 * Альтернатива wp_pagenavi. Создает ссылки пагинации на страницах архивов.
	 *
	 * @param array  $args      Аргументы функции
	 * @param object $wp_query  Объект WP_Query на основе которого строится пагинация. По умолчанию глобальная переменная $wp_query
	 *
	 * @version 2.7
	 * @author  Тимур Камаев
	 * @link    Ссылка на страницу функции: http://wp-kama.ru/?p=8
	*/
	function kama_pagenavi( $args = array(), $wp_query = null ){

		// параметры по умолчанию
		$default = array(
			'before'          => '',   // Текст до навигации.
			'after'           => '',   // Текст после навигации.
			'echo'            => true, // Возвращать или выводить результат.

			'text_num_page'   => '',           // Текст перед пагинацией.
			// {current} - текущая.
			// {last} - последняя (пр: 'Страница {current} из {last}' получим: "Страница 4 из 60").
			'num_pages'       => 10,           // Сколько ссылок показывать.
			'step_link'       => 10,           // Ссылки с шагом (если 10, то: 1,2,3...10,20,30. Ставим 0, если такие ссылки не нужны.
			'dotright_text'   => '…',          // Промежуточный текст "до".
			'dotright_text2'  => '…',          // Промежуточный текст "после".
			'back_text'       => 'Предыдущая',    // Текст "перейти на предыдущую страницу". Ставим 0, если эта ссылка не нужна.
			'next_text'       => 'Следующая',   // Текст "перейти на следующую страницу".  Ставим 0, если эта ссылка не нужна.
			'first_page_text' => '« к началу', // Текст "к первой странице".    Ставим 0, если вместо текста нужно показать номер страницы.
			'last_page_text'  => 'в конец »',  // Текст "к последней странице". Ставим 0, если вместо текста нужно показать номер страницы.
		);

		// Cовместимость с v2.5: kama_pagenavi( $before = '', $after = '', $echo = true, $args = array() )
		if( ($fargs = func_get_args()) && is_string( $fargs[0] ) ){
			$default['before'] = isset($fargs[0]) ? $fargs[0] : '';
			$default['after']  = isset($fargs[1]) ? $fargs[1] : '';
			$default['echo']   = isset($fargs[2]) ? $fargs[2] : true;
			$args              = isset($fargs[3]) ? $fargs[3] : array();
			$wp_query = $GLOBALS['wp_query']; // после определения $default!
		}

		if( ! $wp_query ){
			wp_reset_query();
			global $wp_query;
		}

		if( ! $args ) $args = array();
		if( $args instanceof WP_Query ){
			$wp_query = $args;
			$args     = array();
		}

		$default = apply_filters( 'kama_pagenavi_args', $default ); // чтобы можно было установить свои значения по умолчанию

		$rg = (object) array_merge( $default, $args );

		//$posts_per_page = (int) $wp_query->get('posts_per_page');
		$paged          = (int) $wp_query->get('paged');
		$max_page       = $wp_query->max_num_pages;

		// проверка на надобность в навигации
		if( $max_page <= 1 )
			return false;

		if( empty( $paged ) || $paged == 0 )
			$paged = 1;

		$pages_to_show = intval( $rg->num_pages );
		$pages_to_show_minus_1 = $pages_to_show-1;

		$half_page_start = floor( $pages_to_show_minus_1/2 ); // сколько ссылок до текущей страницы
		$half_page_end   = ceil(  $pages_to_show_minus_1/2 ); // сколько ссылок после текущей страницы

		$start_page = $paged - $half_page_start; // первая страница
		$end_page   = $paged + $half_page_end;   // последняя страница (условно)

		if( $start_page <= 0 )
			$start_page = 1;
		if( ($end_page - $start_page) != $pages_to_show_minus_1 )
			$end_page = $start_page + $pages_to_show_minus_1;
		if( $end_page > $max_page ) {
			$start_page = $max_page - $pages_to_show_minus_1;
			$end_page = (int) $max_page;
		}

		if( $start_page <= 0 )
			$start_page = 1;

		// создаем базу чтобы вызвать get_pagenum_link один раз
		$link_base = str_replace( 99999999, '___', get_pagenum_link( 99999999 ) );
		$first_url = get_pagenum_link( 1 );
		if( false === strpos( $first_url, '?') )
			$first_url = user_trailingslashit( $first_url );

		// собираем елементы
		$els = array();

		if( $rg->text_num_page ){
			$rg->text_num_page = preg_replace( '!{current}|{last}!', '%s', $rg->text_num_page );
			$els['pages'] = sprintf( '<span class="pages">'. $rg->text_num_page .'</span>', $paged, $max_page );
		}
		// назад
		if ( $rg->back_text && $paged != 1 )
			$els['prev'] = '<a class="prev" href="'. ( ($paged-1)==1 ? $first_url : str_replace( '___', ($paged-1), $link_base ) ) .'">'. $rg->back_text .'</a>';
		// в начало
		if ( $start_page >= 2 && $pages_to_show < $max_page ) {
			$els['first'] = '<a class="first" href="'. $first_url .'">'. ( $rg->first_page_text ?: 1 ) .'</a>';
			if( $rg->dotright_text && $start_page != 2 )
				$els[] = '<span class="extend">'. $rg->dotright_text .'</span>';
		}
		// пагинация
		for( $i = $start_page; $i <= $end_page; $i++ ) {
			if( $i == $paged )
				$els['current'] = '<span class="current">'. $i .'</span>';
			elseif( $i == 1 )
				$els[] = '<a href="'. $first_url .'">1</a>';
			else
				$els[] = '<a href="'. str_replace( '___', $i, $link_base ) .'">'. $i .'</a>';
		}

		// ссылки с шагом
		$dd = 0;
		if ( $rg->step_link && $end_page < $max_page ){
			for( $i = $end_page + 1; $i <= $max_page; $i++ ){
				if( $i % $rg->step_link == 0 && $i !== $rg->num_pages ) {
					if ( ++$dd == 1 )
						$els[] = '<span class="extend">'. $rg->dotright_text2 .'</span>';
					$els[] = '<a href="'. str_replace( '___', $i, $link_base ) .'">'. $i .'</a>';
				}
			}
		}
		// в конец
		if ( $end_page < $max_page ) {
			if( $rg->dotright_text && $end_page != ($max_page-1) )
				$els[] = '<span class="extend">'. $rg->dotright_text2 .'</span>';
			$els['last'] = '<a class="last" href="'. str_replace( '___', $max_page, $link_base ) .'">'. ( $rg->last_page_text ?: $max_page ) .'</a>';
		}
		// вперед
		if ( $rg->next_text && $paged != $end_page )
			$els['next'] = '<a class="next" href="'. str_replace( '___', ($paged+1), $link_base ) .'">'. $rg->next_text .'</a>';

		$els = apply_filters( 'kama_pagenavi_elements', $els );

		$out = $rg->before . '<div class="wp-pagenavi">'. implode( ' ', $els ) .'</div>'. $rg->after;

		$out = apply_filters( 'kama_pagenavi', $out );

		if( $rg->echo ) echo $out;
		else return $out;
	}
	
?>