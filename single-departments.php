<?php get_header(); ?>

<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>

<section class="section__margin">
  <div class="container">
    <div class="section__title">
      <h2 class="section__title--main"><?php the_title(); ?></h2>
    </div>
    <div class="section__polyclinic--background">
      <img class="section__polyclinic--img" src="<?php echo get_field('photo'); ?>" alt="<?php the_field( the_title()); ?>" />

      <div class="section__polyclinic_up section__margin">
        <div class="section__polyclinic--header">
          <p class="section__polyclinic--title">Руководитель <span><?php echo get_field('otdelenie'); ?></span></p>
          <div class="section__polyclinic--flex">
            <img src="<?php echo get_field('rukovoditel_foto'); ?>" alt="<?php echo get_field('ruk'); ?>" />
            <div class="">
              <h2><?php echo get_field('ruk'); ?></h2>
              <p>
                <?php echo get_field('rukovoditel_tekst'); ?>
              </p>
            </div>
          </div>
        </div>

        <?php if(get_field('rukovoditel_telefon') !== ''){ ?>
        <div class="section__polyclinic--header">
          <p class="section__polyclinic--title">
            По вопросам госпитализации
          </p>
          <div class="section__polyclinic--flex">
            <div class="">
              <a href="tel:<?php echo get_field('rukovoditel_telefon'); ?>"><?php echo get_field('rukovoditel_telefon'); ?></a>
            </div>
          </div>
        </div>
        <?php } ?>

      </div>

      <div class="section__polyclinic--inside section__margin">
        <div class="section__polyclinic--content">
          
          <?php echo get_field('zagolovka'); ?>
          <br>

         
          
          <?php the_content(); ?> 

          <br />
			 <div class="section__partners section__margin">
				  <div class="container">

					  <div class="section__title">
						            <div class="section__title--main">
          <?php the_field('zagolovok_foto_otdeleniya'); ?>
          </div>
						  
						
					  </div>

					  <div class="partners__slider-2 slider__block">
						  <div class="swiper-wrapper">

	  <?php while(has_sub_field('foto-2')): ?>
						
							<div class="swiper-slide">
								 <img src="<?php the_sub_field('foto_otdeleniya-2'); ?>" alt="" class="foto_otd">
									
							  </div>
							<?php endwhile; ?>



						  </div>
						  <div class="partners__pagination pagination__block"></div>
					  </div>

				  </div>
				</div>

          <div class="section__polyclinic--blocks">
            <?php wp_reset_postdata(); ?>
            <?php foreach(get_field('uslugi') as $uslugi) { ?>
              <?php setup_postdata($uslugi); ?>

              <div class="swiper-slide services__filter--item services__filter__item--active">
                <div class="services__filter--itemblock">
                    <div class="services__filter__item--img">
                      <?php echo get_the_post_thumbnail($uslugi->ID, 'thumbnail'); ?>
                    </div>
                    <div class="services__filter__item--title">
                      <?php //echo $uslugi->post_title; ?>
                      <?php echo get_the_title($uslugi->ID); ?>
                    </div>
                    <a href="<?php echo get_permalink($uslugi->ID); ?>" class="services__filter__item--linck">Подробнее</a>
                </div>
              </div>

            <?php } ?>
            <?php wp_reset_postdata(); ?>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>


<?php 
  get_template_part( 'components/component', 'form');
?>

<?php 
  get_template_part( 'components/component', 'contact');
?>


<?php get_footer(); ?>