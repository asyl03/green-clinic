    <footer>
      <div class="container">
        <div class="footer__row">
          <div class="footer__left">
            <div class="footer__left--logo">
              <a href="/">
                <img src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/footer__logo.svg" alt="Green Clinic" />
              </a>
            </div>
            <div class="footer__left--flex">
              <div class="footer__left--social">
                <div class="social__item">
                  <a href="https://www.instagram.com/greenclinic_astana/">
                    <img src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/instagram--icon.svg" alt="instagram" />
                  </a>
                </div>
                <div class="social__item">
                  <a href="https://api.whatsapp.com/send?phone=77768983800&text=&source=&data=">
                    <img src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/whatsapp--icon.svg" alt="whatsapp" />
                  </a>
                </div>
                <div class="social__item">
                  <a href="https://www.facebook.com/greenclinicastana">
                    <img src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/facebook--icon.svg" alt="facebook" />
                  </a>
                </div>
              </div>
              <div class="footer__left--linck">
                <a class="footer__left--popap">Получить консультацию</a>
              </div>
            </div>
          </div>

         

          <div class="footer__right">
            <?php
              wp_nav_menu(
                array(
                    'theme_location' => 'footermenu4',
                    'container'       => '', 
                    'items_wrap' => '<ul class="menu">%3$s</ul>',
                )
              );
              ?>
          </div>
        </div>
      </div>
	
	
		 <?php if ( is_front_page() ) { ?>
        
        <a class="md-trigger" id="march_id" data-modal="march"></a>
        <div class="md-modal modal_nopadding md-effect-1 md-show" id="march">
            <button class="md-close ">
                <img src="https://greenclinic.kz/wp-content/uploads/2021/06/group-148.png" alt="">
            </button>
            <div class="md-content">
				
				<div class="modal-fon">

					 <img src="https://greenclinic.kz/wp-content/uploads/2021/06/logo-1.svg" alt="" class="modal-img-logo">
					<div class="modal-news-text">
					CHECK-UP (ЧЕКАП)— ЭТО ПРОГРАММА РАННЕЙ ДИАГНОСТИКИ ЗАБОЛЕВАНИЙ
					</div>
					 <a href="https://greenclinic.kz/ru/news/cpeczialnoe-predlozhenie-dlya-sotrudnikov-i-chlenov-semi-aviakompanii/">

					 <img src="https://greenclinic.kz/wp-content/uploads/2021/06/arrow-up.svg" alt="" class="modal-img-arrow">
						 <!--------
						  <img src=" https://greenclinic.kz/wp-content/uploads/2021/06/group-44.png" alt="" class="modal-img-pic">
						------->
					</a> 
				</div>
                
				
            </div>
        </div>
        
    <?php } ?>
	

		
		
		
		
		
	<div class="container">
		 <div class="footer__center">
            <div class="footer__center--title">Услуги</div>
            <div class="row">
              <div class="col-lg-3">
                <?php
                  wp_nav_menu(
                    array(
                        'theme_location' => 'footermenu1',
                        'container'       => '', 
                        'items_wrap' => '<ul class="menu">%3$s</ul>',
                    )
                  );
                ?>
              </div>
				 <div class="col-lg-3">
                <?php
                  wp_nav_menu(
                    array(
                        'theme_location' => 'footermenu1-2',
                        'container'       => '', 
                        'items_wrap' => '<ul class="menu">%3$s</ul>',
                    )
                  );
                ?>
              </div>
              <div class="col-lg-3">
                <?php
                  wp_nav_menu(
                    array(
                        'theme_location' => 'footermenu2',
                        'container'       => '', 
                        'items_wrap' => '<ul class="menu">%3$s</ul>',
                    )
                  );
                ?>
              </div>
              <div class="col-lg-3">
                <?php
                  wp_nav_menu(
                    array(
                        'theme_location' => 'footermenu3',
                        'container'       => '', 
                        'items_wrap' => '<ul class="menu">%3$s</ul>',
                    )
                  );
                ?>
              </div>
            </div>
          </div>
		
	</div>
    </footer>
 <div class="fixed__widget">
        
        <div class="fixed__widget--main fixed__widget--icon">
             <svg width="104" height="118" viewBox="0 0 104 118" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M52 19L86.6411 39V59V79L52 99L17.359 79V39L52 19Z" fill="#85C418"/>
                <g filter="url(#phone__fixed_f)">
                    <path id="phone__fixed--shadow" d="M6.5 32.789L52 6.57703L97.5 32.789V59V85.211L52 111.423L6.5 85.211V32.789Z" stroke="#85C418"/>
                </g>
                <path id="phone__fixed--phone" d="M62.3984 64.1887V67.1887C62.3996 67.4672 62.3425 67.7428 62.2309 67.998C62.1194 68.2532 61.9557 68.4822 61.7505 68.6705C61.5453 68.8588 61.303 69.0021 61.0392 69.0914C60.7754 69.1806 60.4958 69.2137 60.2184 69.1887C57.1413 68.8543 54.1854 67.8028 51.5884 66.1187C49.1723 64.5833 47.1238 62.5348 45.5884 60.1187C43.8984 57.5099 42.8467 54.5396 42.5184 51.4487C42.4934 51.1721 42.5263 50.8934 42.6149 50.6303C42.7036 50.3671 42.846 50.1253 43.0332 49.9203C43.2204 49.7152 43.4482 49.5514 43.7022 49.4392C43.9562 49.327 44.2308 49.2689 44.5084 49.2687H47.5084C47.9937 49.2639 48.4642 49.4357 48.8322 49.7522C49.2002 50.0686 49.4405 50.5081 49.5084 50.9887C49.6351 51.9487 49.8699 52.8914 50.2084 53.7987C50.343 54.1566 50.3721 54.5456 50.2923 54.9195C50.2126 55.2935 50.0273 55.6368 49.7584 55.9087L48.4884 57.1787C49.912 59.6822 51.9849 61.7551 54.4884 63.1787L55.7584 61.9087C56.0303 61.6398 56.3736 61.4545 56.7476 61.3747C57.1215 61.295 57.5105 61.3241 57.8684 61.4587C58.7757 61.7972 59.7184 62.032 60.6784 62.1587C61.1642 62.2272 61.6078 62.4719 61.925 62.8461C62.2421 63.2204 62.4106 63.6982 62.3984 64.1887Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                <defs>
                    <filter id="phone__fixed_f" x="0" y="0" width="104" height="118" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
                    <feGaussianBlur stdDeviation="3" result="effect1_foregroundBlur"/>
                    </filter>
                </defs>
            </svg>

    	</div>
    	
    	
    	<a id="phone_popap" class="fixed__widget--popap fixed__widget--second fixed__widget--icon md-trigger" data-modal="formacall">
    		<svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M16.7941 2.20831V8.20831H22.7941" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M23.7941 1.20831L16.7941 8.20831" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M22.7941 17.1283V20.1283C22.7953 20.4068 22.7382 20.6825 22.6266 20.9376C22.5151 21.1928 22.3514 21.4219 22.1462 21.6102C21.941 21.7984 21.6987 21.9418 21.4349 22.031C21.171 22.1202 20.8915 22.1534 20.6141 22.1283C17.537 21.7939 14.5811 20.7424 11.9841 19.0583C9.56795 17.523 7.51946 15.4745 5.98412 13.0583C4.2941 10.4495 3.24237 7.47928 2.91412 4.38829C2.88913 4.11175 2.922 3.83305 3.01062 3.56991C3.09925 3.30678 3.24169 3.06498 3.42889 2.85991C3.61608 2.65484 3.84393 2.491 4.09791 2.37881C4.3519 2.26662 4.62646 2.20855 4.90412 2.20829H7.90412C8.38943 2.20351 8.85991 2.37537 9.22788 2.69182C9.59585 3.00827 9.8362 3.44773 9.90412 3.92829C10.0307 4.88835 10.2656 5.83101 10.6041 6.73829C10.7387 7.09621 10.7678 7.4852 10.688 7.85917C10.6083 8.23313 10.423 8.5764 10.1541 8.84829L8.88412 10.1183C10.3077 12.6218 12.3806 14.6947 14.8841 16.1183L16.1541 14.8483C16.426 14.5794 16.7693 14.3941 17.1432 14.3144C17.5172 14.2346 17.9062 14.2637 18.2641 14.3983C19.1714 14.7368 20.1141 14.9717 21.0741 15.0983C21.5599 15.1668 22.0035 15.4115 22.3207 15.7858C22.6378 16.1601 22.8063 16.6379 22.7941 17.1283Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
    		<span class="fixed__widget--toltip">
				Заказать звонок
    		     
    		    </span>
	    </a>
    	<a href="tel:+77172790439" id="phone_number" class="fixed__widget--phone fixed__widget--second fixed__widget--icon">
    		<svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M22.1039 17.894V20.894C22.1051 21.1725 22.048 21.4482 21.9364 21.7034C21.8249 21.9586 21.6612 22.1876 21.456 22.3759C21.2508 22.5642 21.0085 22.7075 20.7447 22.7967C20.4809 22.886 20.2013 22.9191 19.9239 22.894C16.8468 22.5597 13.8909 21.5082 11.2939 19.824C8.87776 18.2887 6.82927 16.2402 5.29394 13.824C3.60392 11.2152 2.55218 8.24503 2.22394 5.15403C2.19895 4.8775 2.23181 4.5988 2.32044 4.33566C2.40906 4.07252 2.55151 3.83072 2.7387 3.62566C2.9259 3.42059 3.15374 3.25674 3.40773 3.14456C3.66171 3.03237 3.93628 2.9743 4.21394 2.97403H7.21394C7.69924 2.96926 8.16973 3.14111 8.5377 3.45757C8.90567 3.77402 9.14601 4.21348 9.21394 4.69403C9.34056 5.6541 9.57539 6.59676 9.91394 7.50403C10.0485 7.86196 10.0776 8.25095 9.99784 8.62491C9.91809 8.99888 9.7328 9.34215 9.46394 9.61403L8.19394 10.884C9.61749 13.3876 11.6904 15.4605 14.1939 16.884L15.4639 15.614C15.7358 15.3452 16.0791 15.1599 16.4531 15.0801C16.827 15.0004 17.216 15.0295 17.5739 15.164C18.4812 15.5026 19.4239 15.7374 20.3839 15.864C20.8697 15.9326 21.3133 16.1772 21.6305 16.5515C21.9476 16.9258 22.1161 17.4036 22.1039 17.894Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
    		<span class="fixed__widget--toltip">
				Позвонить
    		   
    		    </span>
	    </a>
    	
    </div>





    <div class="phone__fixed">
        <a>
            <svg width="104" height="118" viewBox="0 0 104 118" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M52 19L86.6411 39V59V79L52 99L17.359 79V39L52 19Z" fill="#85C418"/>
                <g filter="url(#phone__fixed_f)">
                    <path id="phone__fixed--shadow" d="M6.5 32.789L52 6.57703L97.5 32.789V59V85.211L52 111.423L6.5 85.211V32.789Z" stroke="#85C418"/>
                </g>
                <path id="phone__fixed--phone" d="M62.3984 64.1887V67.1887C62.3996 67.4672 62.3425 67.7428 62.2309 67.998C62.1194 68.2532 61.9557 68.4822 61.7505 68.6705C61.5453 68.8588 61.303 69.0021 61.0392 69.0914C60.7754 69.1806 60.4958 69.2137 60.2184 69.1887C57.1413 68.8543 54.1854 67.8028 51.5884 66.1187C49.1723 64.5833 47.1238 62.5348 45.5884 60.1187C43.8984 57.5099 42.8467 54.5396 42.5184 51.4487C42.4934 51.1721 42.5263 50.8934 42.6149 50.6303C42.7036 50.3671 42.846 50.1253 43.0332 49.9203C43.2204 49.7152 43.4482 49.5514 43.7022 49.4392C43.9562 49.327 44.2308 49.2689 44.5084 49.2687H47.5084C47.9937 49.2639 48.4642 49.4357 48.8322 49.7522C49.2002 50.0686 49.4405 50.5081 49.5084 50.9887C49.6351 51.9487 49.8699 52.8914 50.2084 53.7987C50.343 54.1566 50.3721 54.5456 50.2923 54.9195C50.2126 55.2935 50.0273 55.6368 49.7584 55.9087L48.4884 57.1787C49.912 59.6822 51.9849 61.7551 54.4884 63.1787L55.7584 61.9087C56.0303 61.6398 56.3736 61.4545 56.7476 61.3747C57.1215 61.295 57.5105 61.3241 57.8684 61.4587C58.7757 61.7972 59.7184 62.032 60.6784 62.1587C61.1642 62.2272 61.6078 62.4719 61.925 62.8461C62.2421 63.2204 62.4106 63.6982 62.3984 64.1887Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                <defs>
                    <filter id="phone__fixed_f" x="0" y="0" width="104" height="118" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
                    <feGaussianBlur stdDeviation="3" result="effect1_foregroundBlur"/>
                    </filter>
                </defs>
            </svg>
        </a>
    </div>

    <div class="modal__fixed">
      <div class="modal__wrapper">
        <div class="modal__header">
          <h2>Заказать звонок</h2>
            <svg class="modal__close" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M18 6L6 18" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
              <path d="M6 6L18 18" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </div>
		  
        <form id="form_mail" class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="form__input--item">
                <input type="text" name="name" class="form__input" placeholder="Ваше имя">
              </div>
            </div>
            <div class="col-lg-12">
              <div class="form__input--item">
                <input type="phone" name="phone" class="form__input mask" placeholder="Ваш телефон*" required="">
              </div>
            </div>
            <div class="col-lg-12">
              <div class="form__input--item">
                <input type="submit" class="form__input" value="Отправить">
              </div>
            </div>
            <div class="col-lg-12">
              <div class="form__input-bott_desc message">
                Нажмите “Отправить” наши консультанты с вами свяжуться и помогут
              </div>
            </div>
          </div>
        </form>
		
      </div>
    </div>

    <script src="<?php echo esc_url(get_template_directory_uri() ) ?>/js/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="<?php echo esc_url(get_template_directory_uri() ) ?>/js/swiper.min.js" crossorigin="anonymous"></script>

    <script src="<?php echo esc_url(get_template_directory_uri() ) ?>/js/classie.js" crossorigin="anonymous" defer></script>
    <script src="<?php echo esc_url(get_template_directory_uri() ) ?>/js/modalEffects.js" crossorigin="anonymous" defer></script>

    <script src="<?php echo esc_url(get_template_directory_uri() ) ?>/js/jquery.maskedinput.min.js" crossorigin="anonymous"></script>


    <link
      rel="stylesheet"
      href="<?php echo esc_url(get_template_directory_uri() ) ?>/css/jquery.fancybox.min.css"
    />
    <script src="<?php echo esc_url(get_template_directory_uri() ) ?>/js/jquery.fancybox.min.js" defer crossorigin="anonymous"></script>

    <script src="<?php echo esc_url(get_template_directory_uri() ) ?>/js/script.js"></script>


    <script>
      document.addEventListener("DOMContentLoaded", function() {
            var pause = 3500;
            setTimeout(function(){
                
                if($("#map").length){
                    var elem = document.createElement('script');
                    elem.type = 'text/javascript';
                    elem.src = '//api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU&onload=getYaMap';
                    document.getElementsByTagName('body')[0].appendChild(elem);
                }
                
                $('img').each(function(i,elem) {
					var aimg = $(this).data("src");
					$(this).attr("src", aimg);
				});
				$('iframe').each(function(i,elem) {
					var aiframe = $(this).data("src");
					$(this).attr('src',aiframe);
				});

            }, pause);
        });
      

        function getYaMap(){	
            var map_m = new ymaps.Map('map', {
                center: [51.1116698892065,71.43100116841073],
                zoom: 12,
                controls: [],
            });
            
            var polygonPlacemark_m1 = new ymaps.Placemark(
                [51.13524407263706,71.4317494999999],
                {
                    balloonContent: 'г. Нур-Султан, ул. Сарайшык 11/1 (филиал)'
                },
                // {
                //     iconLayout: 'default#image',
                //     //iconLayout: polygonLayout_m,
                //     iconImageHref: '/wp-content/themes/beg/img/map-icon.png',
                //     iconImageSize: [59, 59],
                //     iconImageOffset: [-25, -59]
                // }
            );
            var polygonPlacemark_m2 = new ymaps.Placemark(
                [51.0822395726413,71.40741449999997],
                {
                    balloonContent: 'г. Нур-Султан, ул. Хусейн бен Талал 25/1'
                },
                // {
                //     iconLayout: 'default#image',
                //     //iconLayout: polygonLayout_m,
                //     iconImageHref: '/wp-content/themes/beg/img/map-icon.png',
                //     iconImageSize: [59, 59],
                //     iconImageOffset: [-25, -59]
                // }
            );
            map_m.geoObjects.add(polygonPlacemark_m1);
            map_m.geoObjects.add(polygonPlacemark_m2);


            map_m.behaviors.disable('scrollZoom');
		}
    </script>
    
    <?php //wp_footer(); ?>
  </body>
</html>
