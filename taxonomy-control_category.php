<?php get_header(); ?>

<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>
<?php 
   $control16 = new WP_Query( array(
    'orderby' => 'date', // тип сортировки (в данном случае по дате)
    'order'   => 'ASC',
    'post_type' => 'control',
    'tax_query' => array(
      array (
          'taxonomy' => 'control_category',
          'field' => 'term_id',
          'terms' => get_queried_object()->term_id,
      )
  ),
));
?>

<div class="section__services section__margin">
  <div class="container">
    <div class="services__filter">


      <div class="services__filter--slider">

        <div class="services__filter--content swiper-wrapper">



          <?php while ( $control16->have_posts() ) { $control16->the_post(); ?>

          <?php get_template_part( 'components/component', 'service16' ); ?>

          <?php } ?>

        </div>

      </div>

    </div>



  </div>
</div>

<?php 
  get_template_part( 'components/component', 'contact');
?>


<?php get_footer(); ?>