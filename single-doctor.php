<?php get_header(); ?>

<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>

<section class="section__margin">
    <div class="container">
      <div class="section__title">
        <h2 class="section__title--main"><?php the_title(); ?></h2>
      </div>
    </div>
  </section>

  <section class="section__doctors--inside section__margin">
    <div class="container">
      <div class="content__header">
        <div class="content__header--left">
          <img src="<?php the_post_thumbnail_url(); ?>" alt="person" />
        </div>
        <div class="content__header--right">
          <h2><?php echo get_field('fio'); ?></h2>
          <p><?php echo get_field('professiya'); ?></p>
          <p>
            <strong>Категория: </strong><?php echo get_field('kategoriya'); ?>
          </p>
          <p>
            <b>Опыт работы - <?php echo get_field('opyt_raboty'); ?> лет</b>
            <b><?php echo get_field('kogo_lechit'); ?></b>
          </p>
          <a class="footer__left--popap">Получить консультацию</a>
        </div>
      </div>
      <div class="content__body">
        <div class="services__filter">
          <div class="services__filter--button">
            <div class="services__filter__button--item services__filter__button--active" data-id="10">
              О враче
            </div>
            <div class="services__filter__button--item" data-id="11">
              Опыт работы
            </div>
          </div>

          <div class="services__filter--slider">
            <div class="services__filter--content">
              <!-- Первый блок -->
              <div
                class="services__filter--item services__filter__item--10 services__filter__item--active">
                <div class="">
                <?php echo get_field('o_vrache'); ?>
                </div>
              </div>

              <!-- Второй блок -->
              <div class="services__filter--item services__filter__item--11">
                <div class="">
                  <?php echo get_field('opyt_raboty_tekst'); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>

<?php 
  get_template_part( 'components/component', 'form');
?>

<?php 
  get_template_part( 'components/component', 'contact');
?>
<?php get_footer(); ?>