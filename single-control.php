<?php get_header(); ?>

<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>

<section class="section__margin">
  <div class="container">
    <div class="section__title">
      <h2 class="section__title--main"><?php the_title(); ?></h2>
    </div>
  </div>
</section>

<section class="section__cardiology">
  <div class="container">
    <div class="cardiology__banner">
      <div class="cardiology__banner--img">
        <img src="<?php the_field( 'img_uslugi' ); ?>"
          alt="<?php the_field( the_title()); ?>" />
      </div>
	
      <div class="cardiology__banner--info">

        <?php foreach(get_field('o_cheloveke') as $item){ ?>
          <div class="cardiology__avatar">
            <img src="<?php echo $item['photo']; ?>" alt="avatar" />
          </div>
          <div class="cardiology__banner--content">
            <h2 class="cardiology__banner--title"><?php echo $item['fio']; ?></h2>
            <p class="cardiology__banner--text"><?php echo $item['professiya']; ?></p>
            <span class="cardiology__banner--small">
            <?php echo $item['tekst']; ?>
          </span>
        </div>
        <?php } ?>

      </div>
    </div>
  </div>
</section>

<section class="section__cardiology__up section__margin">
  <div class="container">
    <div class="cardialogy__inside">

      <div class="cardialogy__content">
        <?php echo get_field('tekst_1'); ?>
      </div>

    </div>

    <div class="cardialogy__inside">

      <div class="cardialogy__content">
      <?php echo get_field('tekst_2'); ?>
      </div>
      
    </div>
  </div>
</section>



<?php 
  get_template_part( 'components/component', 'form');
?>

<?php 
  get_template_part( 'components/component', 'contact');
?>
<?php get_footer(); ?>