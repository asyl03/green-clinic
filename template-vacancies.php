<?php
/*
    Template Name: Вакансии
    Template Post Type: page
*/
?>


<?php get_header(); ?>

<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>


<div class="section__margin">
  <div class="container">

    <div class="section__title">
      <div class="section__title--main">
        <?php the_title(); ?>
      </div>
    </div>

  </div>
</div>


<section class="section__vacancies section__margin">
    <div class="container">

        <div class="row">
        
            <div class="col-lg-6">

                <div class="block__vacancie">

                    <div class="block__subtitle">
                        Стать частью команды
                    </div>

                    <?php foreach(get_field('vakansiya') as $item){ ?>
                        <div class="vacancie__item">

                            <div class="vacancie__item--data">
                                Вакансия опубликована <?php echo $item['data_publikaczii']; ?>
                            </div>
                            <div class="vacancie__item--title">
                                <?php echo $item['zagolovok']; ?>
                            </div>
                            <hr>
                            <div class="vacancie__item--info">
                                <div class="vacancie__item--infotitle">
                                    Информация о вакансии
                                </div>
                                <div class="vacancie__info--row">

                                    <?php if($item['informacziya']['tip_zanyatosti'] != ''){ ?>
                                        <div class="vacancie__info--item">
                                            <div class="vacancie__info--left">
                                                Тип занятости
                                            </div>
                                            <div class="vacancie__info--right">
                                                <?php echo $item['informacziya']['tip_zanyatosti']; ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if($item['informacziya']['stazhirovka'] != ''){ ?>
                                        <div class="vacancie__info--item">
                                            <div class="vacancie__info--left">
                                                Стажировка
                                            </div>
                                            <div class="vacancie__info--right">
                                                <?php echo $item['informacziya']['stazhirovka']; ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if($item['informacziya']['grafik_raboty'] != ''){ ?>
                                        <div class="vacancie__info--item">
                                            <div class="vacancie__info--left">
                                                График работы
                                            </div>
                                            <div class="vacancie__info--right">
                                                <?php echo $item['informacziya']['grafik_raboty']; ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if($item['informacziya']['opyt_raboty'] != ''){ ?>
                                        <div class="vacancie__info--item">
                                            <div class="vacancie__info--left">
                                                Опыт работы
                                            </div>
                                            <div class="vacancie__info--right">
                                                <?php echo $item['informacziya']['opyt_raboty']; ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="vacancie__item--linck">
                                <a href="<?php echo $item['ssylka_na_vakansiyu']; ?>" target="_blank" class="vacancie__linck">Подробнее</a>
                            </div>
                        
                        </div>
                    <?php } ?>


                </div>
            
            </div>
            <div class="col-lg-6">
            
                <div class="block__vacancie">

                    <div class="block__subtitle">
                        Хочешь к нам?
                    </div>
                    <div class="block__vacancie--formdesc">
                        Пишите на нашу почту <a href="mailto:info@greenclinic.kz">info@greenclinic.kz</a> или заполни форму:
                    </div>
                    <?php 
                        get_template_part( 'components/component', 'vacancie');
                    ?>

                </div>
            
            </div>
        
        </div>

    </div>
</section>



<?php 
  get_template_part( 'components/component', 'form');
?>

<?php 
  get_template_part( 'components/component', 'contact');
?>


<?php get_footer(); ?>