$(".services__filter__button--item").click(function () {
  var itemid = $(this).data("id")

  $(".services__filter__button--item").removeClass(
    "services__filter__button--active"
  )
  $(this).addClass("services__filter__button--active")

  if ($(window).width() > "993") {
    $(".services__filter--item").removeClass("services__filter__item--active")
    $(".services__filter__item--" + itemid).addClass(
      "services__filter__item--active"
    )
  }
})

$(".content__body.services__filter__button--item").click(function () {
  var itemid = $(this).data("id")

  $(".content__body.services__filter__button--item").removeClass(
    "services__filter__button--active"
  )
  $(this).addClass("services__filter__button--active")

  $(".content__body.services__filter--item").removeClass(
    "services__filter__item--active"
  )
  $(".content__body.services__filter__item--" + itemid).addClass(
    "services__filter__item--active"
  )
})

$(".count").each(function () {
  $(this)
    .prop("Counter", 0)
    .animate(
      {
        Counter: $(this).text(),
      },
      {
        duration: 4000,
        easing: "swing",
        step: function (now) {
          $(this).text(Math.ceil(now))
        },
      }
    )
})

var partners = new Swiper(".partners__slider", {
  slidesPerView: 6,
  slidesPerGroup: 6,
  spaceBetween: 30,
  pagination: {
    el: ".partners__pagination",
    clickable: true,
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      slidesPerGroup: 1,
      spaceBetween: 0,
    },
    480: {
      slidesPerView: 2,
      slidesPerGroup: 2,
      spaceBetween: 30,
    },
    992: {
      slidesPerView: 3,
      slidesPerGroup: 3,
      spaceBetween: 30,
    },
    1024: {
      slidesPerView: 4,
      slidesPerGroup: 4,
      spaceBetween: 30,
    },
  },
})
var partners = new Swiper(".partners__slider-2", {
  slidesPerView: 3,
  slidesPerGroup: 6,
  spaceBetween: 30,
  pagination: {
    el: ".partners__pagination",
    clickable: true,
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      slidesPerGroup: 1,
      spaceBetween: 0,
    },
    540: {
      slidesPerView: 1,
      slidesPerGroup: 2,
      spaceBetween: 30,
    },
    992: {
      slidesPerView: 2,
      slidesPerGroup: 3,
      spaceBetween: 30,
    },
    1024: {
      slidesPerView: 3,
      slidesPerGroup: 4,
      spaceBetween: 30,
    },
  },
})

var partners = new Swiper(".certificate__slider", {
  slidesPerView: 4,
  slidesPerGroup: 4,
  spaceBetween: 30,
  pagination: {
    el: ".certificate__pagination",
    clickable: true,
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      slidesPerGroup: 1,
      spaceBetween: 0,
    },
    480: {
      slidesPerView: 2,
      slidesPerGroup: 2,
      spaceBetween: 30,
    },
    992: {
      slidesPerView: 3,
      slidesPerGroup: 3,
      spaceBetween: 30,
    },
    1024: {
      slidesPerView: 3,
      slidesPerGroup: 3,
      spaceBetween: 30,
    },
  },
})

$(".header__contact--popap, .footer__left--popap, .mobile__menu--active, .phone__fixed").click(() => {
  jQuery(".modal__fixed").toggleClass("modal__fixed--active")
  jQuery(".header__mobile").removeClass("mobile__menu--active")
  jQuery("html").removeClass("body--hidden")
})

$(".modal__close").click(() => {
  jQuery(".modal__fixed").removeClass("modal__fixed--active")
})

$(".mask").mask("+7(999)999-99-99");

$(".click__mobile").click(function () {
  jQuery(".header__mobile").toggleClass("mobile__menu--active")
  jQuery("html").toggleClass("body--hidden")
})

$(".menu-item-has-children > a").click(function (e) {
  // e.preventDefault()
  $(this).parents(".menu-item-has-children").find(".sub-menu").slideToggle()
})

var doctors_click = document.querySelector(".section__doctors--click")
var doctors_active = document.querySelector("#data-doctors-active")
if (doctors_click) {
  doctors_click.addEventListener("click", function () {
    doctors_active.classList.toggle("active")
  })
}

if ($(window).width() < "1025") {
  var advantages = new Swiper(".advantages__slider", {
    slidesPerView: 2,
    slidesPerGroup: 2,
    spaceBetween: 30,
    pagination: {
      el: ".advantages__pagination",
      clickable: true,
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        slidesPerGroup: 1,
        spaceBetween: 0,
      },
      992: {
        slidesPerView: 1,
        slidesPerGroup: 1,
        spaceBetween: 0,
      },
      1024: {
        slidesPerView: 2,
        slidesPerGroup: 2,
        spaceBetween: 30,
      },
    },
  })
}
if ($(window).width() < "993") {
  var services = new Swiper(".services__slider", {
    slidesPerView: 2,
    spaceBetween: 30,
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 0,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 0,
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 10,
      },
      1024: {
        slidesPerView: 3,
        spaceBetween: 10,
      },
    },
  })

  // $(".services__filter--item").hide();
  // $(".services__filter__item--active").show();
  // $(".services__filter--item").addClass("services__filter__item--active")

  $(".services__filter__button--item").click(function () {
    var itemid = $(this).data("id")
    $(".services__filter--item").hide()
    $(".services__filter__item--" + itemid).show()

    setTimeout(function () {
      servicesfilter.update()
      servicesfilter.slideTo(0)
    }, 500)
  })

  var servicesfilter = new Swiper(".services__filter--slider", {
    slidesPerView: 4,
    slidesPerGroup: 4,
    // slidesPerColumn: 2,
    spaceBetween: 15,
    pagination: {
      el: ".services__filter__pagination",
      clickable: true,
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        slidesPerGroup: 1,
        spaceBetween: 15,
        pagination: false,
      },
      480: {
        slidesPerView: 2,
        slidesPerGroup: 2,
        spaceBetween: 15,
      },
      568: {
        slidesPerView: 2,
        slidesPerGroup: 2,
        spaceBetween: 15,
      },
      768: {
        slidesPerView: 3,
        slidesPerGroup: 3,
        spaceBetween: 15,
      },
      992: {
        slidesPerView: 4,
        slidesPerGroup: 4,
        spaceBetween: 15,
      },
      1024: {
        slidesPerView: 4,
        slidesPerGroup: 4,
        spaceBetween: 15,
      },
    },
  })
  
  // setTimeout(function(){
  //   servicesfilter.slideTo(1);
  //   servicesfilter.update();
  //   // $(".services__filter__button--item.services__filter__button--active").trigger('click');
  // }, 1000);
}

jQuery("#form_mail").on("submit", function(){

  var formNm = jQuery('#form_mail');
  jQuery.ajax({
		url: '/wp-content/themes/GreenClinic/mail.php',
		type: 'post',
		data: formNm.serialize(),
		success: function(data){
			formNm.find('.message').html(data);
		}
	});

  return false;
});
jQuery("#form_mail-1").on("submit", function(){

  var formNm = jQuery('#form_mail-1');
  jQuery.ajax({
		url: '/wp-content/themes/GreenClinic/mail.php',
		type: 'post',
		data: formNm.serialize(),
		success: function(data){
			formNm.find('.message').html(data);
		}
	});

  return false;
});




$(function() {
  document.getElementById('form_mail-vacancie').addEventListener('submit', function(evt){
    var http = new XMLHttpRequest(), f = this;
    var th = $(this);
    evt.preventDefault();
    http.open("POST", "/wp-content/themes/GreenClinic/mail-file.php", true);
    http.onreadystatechange = function() {
      if (http.readyState == 4 && http.status == 200) {
        if (http.responseText.indexOf(f.name.value) == 0) { // очистить поля формы, если в ответе первым словом будет имя отправителя (name)
          th.trigger("reset");
          th.find('.message').html('<center><b>Сообщение отправлено успешно!</b></center>');
        }
      }
    }
    http.onerror = function() {
      alert('Ошибка, попробуйте еще раз');
    }
    http.send(new FormData(f));
  }, false);
 
});

// jQuery("#form_mail-vacancie").on("submit", function(){

//   var formNm = jQuery('#form_mail-vacancie');
//   var formData = new FormData(this);

//   jQuery.ajax({
// 		url: '/wp-content/themes/GreenClinic/mail-file.php',
// 		type: 'post',
// 		// data: formNm.serialize(),
//     cache: false,
//     contentType: false,
//     processData: false,
//     data: formData,

// 		success: function(data){
// 			formNm.find('.message').html(data);
// 		}
// 	});

//   return false;
// });



jQuery(".share__more__item a").click(function(){
    event.preventDefault();
    var url = jQuery(this).data("href");
    var title = 'Поделиться';
    var w = 800;
    var h = 400;
    var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;
    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 3) - (h / 3)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    if (window.focus) {
            newWindow.focus();
    }
});



$("#thisPh").change(function() {
  if($(this)[0].files[0]){
      var filename  = $(this)[0].files[0].name;
      $('label[for="thisPh"] .file-text').text(filename);
      console.log(filename);
  }else{
      $('label[for="thisPh"] .file-text').text('Можете прикрепить резюме размером не более 10 МБ');
  }
});


let stat = false;
function openIcon() {
    jQuery('.fixed__widget--popap').animate({
        top: '-250',
        opacity: 1,
		
    }, 400);
    jQuery('.fixed__widget--phone').animate({
        top: '-170',
        opacity: 1,
    }, 400);
    jQuery('.fixed__widget--second .fixed__widget--toltip').animate({
        width: 'show'
    }, 350);
    stat = true;
};
function closeIcon() {
    stat = false;
    jQuery('.fixed__widget--second .fixed__widget--toltip').animate({
        width: 'hide'
    }, 350);
    jQuery('.fixed__widget--second').animate({
        top: '0',
        opacity: 0,
    }, 400);
};


// jQuery('.fixed__widget').on('click', function () {
//     openIcon();
// });
jQuery('.fixed__widget--main').on('click', function () {
    if (stat == true) {
        stat = false;
        closeIcon();
    } else {
        openIcon();
    }
});
jQuery(document).on('click', function (e) {
    if (!e.target.closest('.fixed__widget')) {
      closeIcon();
    }
});
