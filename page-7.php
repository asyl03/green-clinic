<?php get_header(); ?>


<div class="page__banner--bread">
        <div class="bread-line">
            <div class="container">
            <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
            </div>
        </div>
    </div>

    <div class="section__margin">
        <div class="container">

            <div class="section__title">
                <div class="section__title--main">
                <?php the_title(); ?>
                </div>
            </div>

            <div class="page__about">
                <div class="row">

                    <div class="col-lg-6 order-2 order-lg-1">
                        <div class="page__blocktext">
                        <?php the_content(); ?>   
                        </div>
    
                    </div>
                    <div class="col-lg-6 order-1 order-lg-2">
    
                        <div class="page__about--img">
                            <img src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/about_img.jpg" alt="Green Clinic в Нур-Султане">
                        </div>
                        
                    </div>
    
                </div>
            </div>
            
        </div>
    </div>
	<div class="section__margin">
        <div class="container">
            <div class="row">

                    <div class="col-lg-6 ">
						 <div class="page__about--img page__about--img-2">
							 <img src="<?php the_field( 'obr_img' ); ?>">
                            
                        </div>
                        
    
                    </div>
                    <div class="col-lg-6 ">
    
                       <div class="page__blocktext">
                         <?php echo get_field('obr_tekst'); ?> 
                        </div>
                        
                    </div>
    
                </div>
        </div>
    </div>
    <div class="section__margin">
        <div class="container">
            <div class="page__about--mission">
                <div class="about__mission--title">
                    <?php echo get_field('missiya_zagolovok'); ?>
                </div>
                <div class="about__mission--text">
                <?php echo get_field('missiya_tekst'); ?>       
                </div>
            </div>
        </div>
    </div>

    <?php 
      get_template_part( 'components/component', 'advantage');
    ?>

    <div class="section__margin">
        <div class="container">

            <div class="section__title">
                <div class="section__title--main">
                <?php echo get_field('zagolovok_sertifikat'); ?>
                </div>
            </div>


            <div class="certificate__slider slider__block">
                <div class="swiper-wrapper">


                <?php foreach(get_field('sertifikat') as $item){ ?>
                  <div class="swiper-slide">
                        <div class="certificate__item">
                            <a data-fancybox="gallery" href="<?php echo $item['foto']; ?>">
                                <img src="<?php echo $item['foto']; ?>" alt="Сертификат">
                            </a>
                        </div>
                    </div>
                  <?php } ?>

                    

                </div>
                <div class="certificate__pagination pagination__block"></div>
            </div>


        </div>
    </div>


    <?php 
      get_template_part( 'components/component', 'partners');
    ?>

    <?php 
      get_template_part( 'components/component', 'contact');
    ?>

    <?php 
      get_template_part( 'components/component', 'form');
    ?>


<?php get_footer(); ?>