<?php get_header(); ?>

<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>

<section class="section__margin">
  <div class="container">
    <div class="section__title">
      <h2 class="section__title--main">
        <?php the_title(); ?>
      </h2>
      <time class="section__title--time"><?php the_time('d.m.Y'); ?></time>
    </div>
  </div>
</section>


<section class="section__news section__margin">
      <div class="container">
        <div class="section__news--inside">
			<div class="section__news--inside-banner-img">
          <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field( the_title()); ?>" />
			</div>
          <div class="">
            <?php the_content(); ?>

            <br />
            <br />
            <p>Поделиться:</p>
            <br />
            <ul class="section__news--links">
              <li class="share__more__item">
                <a data-href="https://vk.com/share.php?url=<?php the_permalink(); ?>" target="_blank" class="share__item"><img style="width: 32px; height: 32px" src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/logo-vk.svg" alt="" /></a>
              </li>
              <li class="share__more__item">
                <a data-href="https://twitter.com/share?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>&hashtags=my_hashtag" target="_blank" class="share__item"><img style="width: 32px; height: 32px" src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/logo-twitter.svg" alt="" /></a>
              </li>
              <li class="share__more__item">
                <a data-href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" class="share-link share-link--facebook" target="_blank" class="share__item"><img style="width: 32px; height: 32px" src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/logo-facebook.svg" alt="" /></a>
              </li>
              
            </ul>
          </div>
        </div>
      </div>
    </section>



<?php 
  get_template_part( 'components/component', 'contact');
?>

<?php 
  get_template_part( 'components/component', 'form');
?>

<?php get_footer(); ?>