<?php get_header(); ?>
 
<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>

    <div class="section__services section__margin">
        <div class="container">

            <div class="section__title">
                <div class="section__title--main">
                    Услуги
                </div>
                <div class="section__title--desc">
                    Высокопрофильные специалисты не оставят вас в трудную минуту
                </div>
            </div>


            <?php 
                get_template_part( 'components/component', 'service');
            ?>


            <div class="services__filter">
                <div class="services__filter--button">
                    <div class="services__filter__button--item services__filter__button--active" data-id="16">
                        Поликлиника
                    </div>
                    <div class="services__filter__button--item" data-id="17">
                        Стационар
                    </div>
                    <div class="services__filter__button--item" data-id="18">
                        Отделение лучевой диагностики 
                    </div>
                </div>

                <div class="services__filter--slider">
                    
                    <div class="services__filter--content swiper-wrapper">

                    <?php 
                        $control16 = new WP_Query( array(
                            'orderby' => 'date', // тип сортировки (в данном случае по дате)
							'posts_per_page' => -1,
                            'order'   => 'ASC',
                            'post_type' => 'control',
                            'tax_query' => array(
                                array (
                                    'taxonomy' => 'control_category',
                                    'field' => 'term_id',
                                    'terms' => 16
                                )
                            ),
                        ));
                        $control17 = new WP_Query( array(
                            'orderby' => 'date', // тип сортировки (в данном случае по дате)
							'posts_per_page' => -1,
                            'order'   => 'ASC',
							
                            'post_type' => 'control',
                            'tax_query' => array(
                                array (
                                    'taxonomy' => 'control_category',
                                    'field' => 'term_id',
                                    'terms' => 17
                                )
                            ),
                        ));
                        $control18 = new WP_Query( array(
                            'orderby' => 'date', // тип сортировки (в данном случае по дате)
							'posts_per_page' => -1,
                            'order'   => 'ASC',
                            'post_type' => 'control',
                            'tax_query' => array(
                                array (
                                    'taxonomy' => 'control_category',
                                    'field' => 'term_id',
                                    'terms' => 18
                                )
                            ),
                        ));
                    ?>

                    <?php while ( $control16->have_posts() ) { $control16->the_post(); ?>
                            
                        <?php get_template_part( 'components/component', 'service16' ); ?>
                       
                    <?php } ?>

                    <?php while ( $control17->have_posts() ) { $control17->the_post(); ?>
                            
                        <?php get_template_part( 'components/component', 'service17' ); ?>
                        
                    <?php } ?>

                    <?php while ( $control18->have_posts() ) { $control18->the_post(); ?>
                            
                        <?php get_template_part( 'components/component', 'service18' ); ?>
                        
                    <?php } ?>

                    </div>
                    <div class="services__filter__pagination pagination__block"></div>

                </div>
                
            </div>



        </div>
    </div>

   
 

   

    <?php 
      get_template_part( 'components/component', 'contact');
    ?>

    <?php get_footer(); ?>