<!DOCTYPE html>
<html <?php language_attributes(); ?> >
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	  <meta name="yandex-verification" content="87b80dfa210892e8" />
	  <meta name="google-site-verification" content="ZpDv2bfMO9cv6hV9dVJxY6Kl3OjyDi7RYCG1PfNx4us" />
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title><?php bloginfo('name'); ?><? //wp_title(':'); ?></title>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo esc_url(get_template_directory_uri() ) ?>/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo esc_url(get_template_directory_uri() ) ?>/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo esc_url(get_template_directory_uri() ) ?>/img/favicon-16x16.png">
    <link rel="manifest" href="<?php echo esc_url(get_template_directory_uri() ) ?>/img/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

   
    <?php wp_head(); ?>

    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri() ) ?>/css/bootstrap.min.css" />
    <!-- <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri() ) ?>/css/component.css" /> -->
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri() ) ?>/css/swiper.min.css" />

    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri() ) ?>/css/style.css?v=0.0.1" />
    <!-- <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri() ) ?>/css/media.css?v=0.0.1" /> -->
	  <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
      (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
      m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
      (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

      ym(76952473, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
      });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/76952473" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
	  <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-NMRWS8KV9M"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-NMRWS8KV9M');
	</script>
  </head>
  <body>

    <header>
      <div class="container">
        <div class="header__row">
          <div class="header__logo">
            <a href="/" class="logo">
              <img src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/logo.svg" alt="Green Clinic" />
            </a>
          </div>
          <div class="header__right">
            <div class="header__menu">
              <!-- <ul class="menu">
                
                <li>
                  <a href="#">О нас</a>
                </li>
                <li>
                  <a href="#">Новости</a>
                </li>
                <li class="menu-item-has-children">
                  <a href="#">Отделения</a>
                  <ul class="sub-menu">
                    <li><a href="#">Поликлиника</a></li>
                    <li><a href="#">Стационар</a></li>
                    <li><a href="#">Отделение лучевой диагностики</a></li>
                  </ul>
                </li>
                <li>
                  <a href="#">Закупки</a>
                </li>
                <li>
                  <a href="#">Документация</a>
                </li>
                <li>
                  <a href="#">Контакты</a>
                </li>
              </ul> -->

              <?php
                  wp_nav_menu(
                      array(
                          'theme_location' => 'mainmenu',
                          'container'       => '', 
                          'items_wrap' => '<ul class="menu">%3$s</ul>',
                      )
                  );
              ?>
            </div>
            <div class="header__contact">
              <div class="header__contact--icon">
                <svg
                  width="49"
                  height="49"
                  viewBox="0 0 49 49"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M44.9166 34.545V40.67C44.919 41.2386 44.8025 41.8014 44.5747 42.3224C44.3469 42.8434 44.0128 43.3111 43.5938 43.6955C43.1748 44.0799 42.6801 44.3725 42.1415 44.5547C41.6029 44.7369 41.0321 44.8045 40.4658 44.7533C34.1833 44.0707 28.1484 41.9239 22.8462 38.4854C17.9132 35.3508 13.7309 31.1684 10.5962 26.2354C7.14576 20.9091 4.99847 14.8449 4.32831 8.53416C4.27729 7.96957 4.34439 7.40055 4.52533 6.86331C4.70627 6.32608 4.9971 5.8324 5.37929 5.41372C5.76148 4.99504 6.22666 4.66053 6.74522 4.43148C7.26377 4.20243 7.82434 4.08386 8.39123 4.08333H14.5162C15.5071 4.07358 16.4676 4.42445 17.2189 5.07054C17.9702 5.71663 18.4609 6.61386 18.5996 7.59499C18.8581 9.55513 19.3375 11.4797 20.0287 13.3321C20.3034 14.0628 20.3629 14.857 20.2 15.6205C20.0372 16.3841 19.6589 17.0849 19.11 17.64L16.5171 20.2329C19.4235 25.3443 23.6557 29.5765 28.7671 32.4829L31.36 29.89C31.9151 29.3411 32.6159 28.9628 33.3794 28.7999C34.1429 28.6371 34.9371 28.6966 35.6679 28.9712C37.5202 29.6625 39.4448 30.1419 41.405 30.4004C42.3967 30.5403 43.3025 31.0399 43.95 31.804C44.5975 32.5682 44.9415 33.5437 44.9166 34.545Z"
                    stroke="#0F1721"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>
              </div>
              <div class="header__contact--info">
                <a href="tel:<?php echo get_field('shapka_nomer_telefon',12); ?>" class="header__contact--phone"
                  ><?php echo get_field('shapka_nomer_telefon',12); ?></a
                >
                <a class="header__contact--popap">Заказать звонок</a>
              </div>
            </div>
            <div class="header__right--row">
              <div class="header__right--version">
                <a class="version_visually">Версия для слабовидящих</a>
              </div>
              <div class="header__lang">
                <?php echo my_site_custom_languages_selector_template(); ?>
              </div>
            </div>
            <div class="header__right--click">
              <a class="click__mobile">
                <img src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/menu_click.svg" alt="" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div class="header__mobile">
      <div class="header__mobile--top">
        <div class="header__contact">
          <div class="header__contact--icon">
            <svg
              width="49"
              height="49"
              viewBox="0 0 49 49"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M44.9166 34.545V40.67C44.919 41.2386 44.8025 41.8014 44.5747 42.3224C44.3469 42.8434 44.0128 43.3111 43.5938 43.6955C43.1748 44.0799 42.6801 44.3725 42.1415 44.5547C41.6029 44.7369 41.0321 44.8045 40.4658 44.7533C34.1833 44.0707 28.1484 41.9239 22.8462 38.4854C17.9132 35.3508 13.7309 31.1684 10.5962 26.2354C7.14576 20.9091 4.99847 14.8449 4.32831 8.53416C4.27729 7.96957 4.34439 7.40055 4.52533 6.86331C4.70627 6.32608 4.9971 5.8324 5.37929 5.41372C5.76148 4.99504 6.22666 4.66053 6.74522 4.43148C7.26377 4.20243 7.82434 4.08386 8.39123 4.08333H14.5162C15.5071 4.07358 16.4676 4.42445 17.2189 5.07054C17.9702 5.71663 18.4609 6.61386 18.5996 7.59499C18.8581 9.55513 19.3375 11.4797 20.0287 13.3321C20.3034 14.0628 20.3629 14.857 20.2 15.6205C20.0372 16.3841 19.6589 17.0849 19.11 17.64L16.5171 20.2329C19.4235 25.3443 23.6557 29.5765 28.7671 32.4829L31.36 29.89C31.9151 29.3411 32.6159 28.9628 33.3794 28.7999C34.1429 28.6371 34.9371 28.6966 35.6679 28.9712C37.5202 29.6625 39.4448 30.1419 41.405 30.4004C42.3967 30.5403 43.3025 31.0399 43.95 31.804C44.5975 32.5682 44.9415 33.5437 44.9166 34.545Z"
                stroke="#0F1721"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </div>
          <div class="header__contact--info">
            <a href="tel:<?php echo get_field('shapka_nomer_telefon',12); ?>" class="header__contact--phone"
              ><?php echo get_field('shapka_nomer_telefon',12); ?></a
            >
            <a class="header__contact--popap">Заказать звонок</a>
          </div>
        </div>
        <div class="header__right--click">
          <a class="click__mobile">
            <img src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/close__icon.svg" alt="" />
          </a>
        </div>
      </div>
      <div class="header__mobile--body">
        <?php
          wp_nav_menu(
              array(
                  'theme_location' => 'mainmenu',
                  'container'       => '', 
                  'items_wrap' => '<ul class="menu">%3$s</ul>',
              )
          );
        ?>
      </div>
    </div>
