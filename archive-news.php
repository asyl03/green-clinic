<?php get_header(); ?>

<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>

<div class="section__margin">
  <div class="container">

    <div class="section__title">
      <div class="section__title--main">
        Новости
      </div>
    </div>

  </div>
</div>

<section class="section__new section__margin">
  <div class="container">
    <div class="section__news--grid">
    <?php 
        $media_news = new WP_Query( array(
            'orderby' => 'date', // тип сортировки (в данном случае по дате)
            'post_type' => 'news',
        ));
    ?>
    <?php while ( $media_news->have_posts() ) { $media_news->the_post(); ?>
      <?php get_template_part( 'components/component', 'news' ); ?>
    <?php } ?>

    </div>

    <div class="pagination-row">
      <?php 
          echo paginate_links( array(
              'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
              'total'        => $media_news->max_num_pages,
              'current'      => max( 1, get_query_var( 'paged' ) ),
              'format'       => '?paged=%#%',
              'show_all'     => false,
              'end_size'     => 2,
              'mid_size'     => 1,
              'prev_next'    => false,
              'add_args'     => false,
              'add_fragment' => '',
              'type' => 'list'
          ) );
      ?>
      </div>

  </div>
</section>


<?php 
  get_template_part( 'components/component', 'form');
?>

<?php 
  get_template_part( 'components/component', 'contact');
?>


<?php get_footer(); ?>