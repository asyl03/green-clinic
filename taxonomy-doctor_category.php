<?php get_header(); ?>

<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>

<section class="section__doctors section__margin">
  <div class="container">
    <div class="section__doctors--flex">

      <aside class="left">
        <div class="section__doctors--click">Врачи <span>x</span></div>
        <?php
          wp_nav_menu(
            array(
                'theme_location' => 'sortdoctors',
                'container'       => '', 
                'items_wrap' => '<ul class="" id="data-doctors-active">%3$s</ul>',
            )
          );
          ?>
      </aside>
      <div class="right">
        <?php 
          $doctors = new WP_Query( array(
            'orderby' => 'date', // тип сортировки (в данном случае по дате)
            'order'   => 'ASC',
            'post_type' => 'doctor',
            'tax_query' => array(
              array (
                  'taxonomy' => 'doctor_category',
                  'field' => 'term_id',
                  'terms' => get_queried_object()->term_id,
              )
            ),
            'paged' => get_query_var('paged') ?: 1 // страница пагинации
          ));
        ?>
        <?php while ( $doctors->have_posts() ) { $doctors->the_post(); ?>
                          
          <?php get_template_part( 'components/component', 'doctor' ); ?>
            
        <?php } ?>
        
      </div>

    </div>

    <?php
      kama_pagenavi( $before = ' ', $after = ' ', $echo = true, $args = array(), $wp_query = $doctors);
    ?>

  </div>
</section>



<?php 
  get_template_part( 'components/component', 'contact');
?>


<?php get_footer(); ?>