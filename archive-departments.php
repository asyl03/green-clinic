<?php get_header(); ?>

<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>


<div class="section__margin">
  <div class="container">

    <div class="section__title">
      <div class="section__title--main">
        Отделения
      </div>
    </div>

    <div class="departments__block">

      
        <?php 
          $media_department = new WP_Query( array(
              'orderby' => 'date', // тип сортировки (в данном случае по дате)
              'post_type' => 'departments',
          ));
        ?>

        <?php while ( $media_department->have_posts() ) { $media_department->the_post(); ?>
          <?php get_template_part( 'components/component', 'departments' ); ?>
        <?php } ?>

    </div>


  </div>
</div>

<?php 
  get_template_part( 'components/component', 'contact');
?>


<?php get_footer(); ?>