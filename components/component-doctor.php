<div class="right--block">
  <div class="background">
    <img   src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field( the_title()); ?>" />
  </div>
  <h2><?php echo get_field('fio'); ?></h2>
  <p>
    <?php echo get_field('kategoriya'); ?>
  </p>
  <a href="<?php echo get_permalink(); ?>">Узнать больше</a>
</div>