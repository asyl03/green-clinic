<form id="form_mail-vacancie" class="vacancie__form" enctype="multipart/form-data" method="post">
    <div class="row">

        <div class="col-lg-12">

            <div class="form__input--row">

                <div class="form__input--item">
                    <input type="text" name="name" class="form__input" placeholder="Ваше имя">
                </div>
                <div class="form__input--item">
                    <input type="text" name="mail" class="form__input" placeholder="Ваше e-mail">
                </div>
                <div class="form__input--item">
                    <input type="text" name="phone" class="form__input mask" placeholder="Ваш телефон">
                </div>
            
            </div>

        </div>
        <div class="col-lg-12">
            <div class="form__input--item">
                <textarea name="comment" rows="10" class="form__input" placeholder="Ваше сообщение"></textarea>
            </div>
        </div>

        <div class="col-lg-12">
            <label for="thisPh">
                <span class="file-icon">
                    <svg width="54" height="54" viewBox="0 0 54 54" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M40.5 13.4999C45.153 18.1529 45.153 25.722 40.5 30.375L28.6875 42.1875C28.0654 42.8096 27.0585 42.8083 26.4376 42.1875C25.8166 41.5664 25.8153 40.5595 26.4374 39.9374L38.25 28.1249C41.661 24.7138 41.6621 19.1621 38.25 15.75C34.8379 12.3379 29.2861 12.3389 25.8751 15.75L13.5 28.1251C11.3288 30.2963 11.3288 33.8287 13.5 35.9998C15.6714 38.1712 19.2038 38.1712 21.3749 36L32.6251 24.7499C33.5554 23.8196 33.5556 22.3053 32.6251 21.3749C31.6946 20.4444 30.1804 20.4446 29.2501 21.3749L18.5625 32.0625C17.9404 32.6846 16.9335 32.6833 16.3126 32.0625C15.6916 31.4414 15.6903 30.4345 16.3124 29.8124L27 19.1248C29.1712 16.9536 32.7038 16.9538 34.875 19.125C37.0461 21.2962 37.0463 24.8288 34.8752 26.9999L23.625 38.2501C20.2142 41.661 14.6622 41.6622 11.2499 38.2499C7.83782 34.8378 7.83905 29.2859 11.2499 25.875L23.625 13.4999C28.278 8.84692 35.847 8.84692 40.5 13.4999Z" fill="#474747"/>
                    </svg>
                </span>
                <span class="file-text">
                    Можете прикрепить резюме размером не более 10 МБ
                </span>
            </label>
            <input id="thisPh" type="file" name="file" style="display: none;">
        </div>
        <div class="col-lg-12">
            <div class="form__input--item">
                <input type="submit" class="form__input" value="Отправить">
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form__input-bott_desc message"></div>
        </div>

    </div>
</form>