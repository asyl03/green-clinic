<div class="section__partners section__margin">
  <div class="container">

      <div class="section__title">
          <div class="section__title--main">
          <?php echo get_field('zagolovok_partnery',7); ?>
          </div>
      </div>

      <div class="partners__slider slider__block">
          <div class="swiper-wrapper">


          <?php foreach(get_field('partnery_slajder',7) as $item){ ?>
            <div class="swiper-slide">
                  <a href="<?php echo $item['ssylka_partnera']; ?>" class="partners__item">
                      <img   src="<?php echo $item['iconpartner']; ?>" alt="<?php echo $item['title']; ?>">
                  </a>
              </div>
            <?php } ?>

              

          </div>
          <div class="partners__pagination pagination__block"></div>
      </div>

  </div>
</div>