
<div class="section__contact section__margin">
  <div class="container">

    <div class="row">
      <div class="col-lg-5">

        <div class="section__title">
          <div class="section__title--main">
          КОНТАКТЫ
          </div>
        </div>

        <div class="contact__info">

          <div class="contact__info--item">
            <div class="contact__item--left">
              <div class="contact__item--icon">
                <img   src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/map__icon.svg" alt="">
              </div>
            </div>
            <div class="contact__item--right">
              <div class="contact__item--title">
              <?php echo get_field('zagolovok_adres',12); ?>
              </div>
              <div class="contact__item--text">
                <?php foreach(get_field('kontent_adres', 12) as $item){ ?>
                  <p><?php echo $item['adres']; ?></p>
                <?php } ?>
              </div>
            </div>
          </div>


          <div class="contact__info--item">
            <div class="contact__item--left">
              <div class="contact__item--icon">
                <img   src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/phone__icon.svg" alt="">
              </div>
            </div>
            <div class="contact__item--right">
              <div class="contact__item--title">
                <?php echo get_field('zagolovok_telefon',12); ?>
              </div>
              <div class="contact__item--text">
                <?php foreach(get_field('kontent_telefon',12) as $item){ ?>
                <p><a href="tel:<?php echo $item['telefon']; ?>"><?php echo $item['nomer_telefona']; ?></a></p>
                <?php } ?>
              </div>
            </div>
          </div>

          <div class="contact__info--item">
            <div class="contact__item--left">
              <div class="contact__item--icon">
                <img   src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/calendar__icon.svg" alt="">
              </div>
            </div>
            <div class="contact__item--right">
              <div class="contact__item--text contact__item--calendar">
                <?php echo get_field('kontent_raboty',12); ?>
              </div>
            </div>
          </div>

          <?php foreach(get_field('kontent_el_pochty',12) as $item){ ?>
          <div class="contact__info--item">
            <div class="contact__item--left">
              <div class="contact__item--icon">
                <img   src="<?php echo esc_url(get_template_directory_uri() ) ?>/img/mail__icon.svg" alt="">
              </div>
            </div>
            <div class="contact__item--right">
              <div class="contact__item--title">
                <?php echo get_field('zagolovok_el_pochta',12); ?>
              </div>
              <div class="contact__item--text">
                <p><a href="mailto:<?php echo $item['email']; ?>"><?php echo $item['email']; ?></a></p>
              </div>
            </div>

          </div>
          <?php } ?>



        </div>


      </div>
      <div class="col-lg-7">

        <div class="contact__map" id="map" style="height: 400px; width: 100%;">
        <!-- <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ae0046e2fa6c74108b14e0774219ef68f3f0b85d50d9dd5fad70129dc5a2f06e8&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script> -->
        </div>

      </div>
    </div>
  </div>
</div>