<a href="<?php echo get_permalink(); ?>" class="section__news--block">
  <img   src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field( the_title()); ?>" />
  <time><?php the_time('d.m.Y'); ?></time>
  <p><?php the_title(); ?></p>
</a>