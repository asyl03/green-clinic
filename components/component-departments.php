<div class="departments__item">
  <div class="row">
    <div class="col-lg-6">
      <div class="departments__item--img">
        <img   src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field( the_title()); ?>">
      </div>
    </div>
    <div class="col-lg-6">
      <div class="departments__item--info">
        <div class="departments__item--top">
          <div class="departments__item--title">
          <?php the_title(); ?>
          </div>
          <div class="departments__item--text">
            <?php echo get_field('tekst'); ?>
          </div>
        </div>
        <div class="departments__item--linck">
          <a href="<?php echo get_permalink(); ?>">Узнать больше</a>
        </div>
      </div>
    </div>
  </div>
</div>