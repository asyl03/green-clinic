<?php 
  $category = get_the_category(get_the_ID());
  $cat_id = get_cat_ID($category[0]->cat_name);
?>
<div class="swiper-slide services__filter--item services__filter__item--<?php echo $cat_id; ?> services__filter__item--active">
  <div class="services__filter--itemblock">
      <div class="services__filter__item--img">
          <img   src="<?php the_post_thumbnail_url(); ?>" alt="">
          
      </div>
      <div class="services__filter__item--title">
      <?php the_title(); ?>
      </div>
      <a href="<?php echo get_permalink(); ?>" class="services__filter__item--linck">Подробнее</a>
  </div>
</div>