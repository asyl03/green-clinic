<div class="swiper-slide services__filter--item services__filter__item--16 services__filter__item--active">
  <div class="services__filter--itemblock">
      <div class="services__filter__item--img">
          <!-- <img src="<?php //the_post_thumbnail_url(); ?>" alt="123"> -->
          <?php the_post_thumbnail('full', array( 'alt' => get_the_title() )); ?>
      </div>
      <div class="services__filter__item--title">
      <?php the_title(); ?>
      </div>
      <a href="<?php echo get_permalink(); ?>" class="services__filter__item--linck">Подробнее</a>
  </div>
</div>