<div class="services__number--row services__slider">
  <div class="row swiper-wrapper" id="counter">
  <?php foreach(get_field('uslugi',231) as $item){ ?>
    <div class="col-xl-3 swiper-slide">
      <div class="services__number--item">
        <div class="services__number--number count" data-number="<?php echo $item['czifra']; ?>">
          <?php echo $item['czifra']; ?>
        </div>
        <div class="services__number--text">
          <?php echo $item['tekst']; ?>
        </div>
      </div>
    </div>
  <?php } ?>
      
  </div>
</div>