<div class="section__margin">
    <div class="advantages__slider">
      <div class="row swiper-wrapper">

        <?php foreach(get_field('preimushhestvo',7) as $item){ ?>
        <div class="col-xl-3 swiper-slide">
          <div class="advantage__item">
            <div class="advantage__item--img">
              <img   src="<?php echo $item['icon']; ?>" alt="  <?php echo $item['title']; ?>">
            </div>
            <div class="advantage__item--info">
              <div class="advantage__item--title">
                <?php echo $item['title']; ?>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>

      </div>
      <div class="advantages__pagination pagination__block"></div>
    </div>
</div>