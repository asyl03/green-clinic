<div class="section__form section__margin">
  <div class="consultant__block">
    <div class="container">
      <div class="consultant__form">

        <div class="consultant__form--title">
          Получить консультацию прямо сейчас
        </div>
        <form id="form_mail-1">
          <div class="row">

            <div class="col-lg-4">
              <div class="form__input--item">
                <input type="text" name="name" class="form__input" placeholder="Ваше имя">
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form__input--item">
                <input type="text" name="phone" class="form__input mask" placeholder="Ваш телефон*" required>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="form__input--item">
                <input type="submit" class="form__input" value="Отправить">
              </div>
            </div>

            <div class="col-lg-12">
              <div class="form__input-bott_desc message">
                Нажмите “Отправить” наши консультанты с вами свяжуться и помогут
              </div>
            </div>

          </div>
        </form>

      </div>
    </div>
  </div>
</div>