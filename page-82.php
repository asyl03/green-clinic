<?php get_header(); ?>

<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>

<section class="section__margin">
  <div class="container">
    <div class="section__title">
      <h2 class="section__title--main"><?php the_title(); ?></h2>
    </div>
  </div>
</section>


<section class="section__doc section__margin">
      <div class="container">
        <div class="section__docs--grid">

        <?php foreach(get_field('dokumenty') as $item){ ?>
          <a href="<?php echo $item['fajl_skachat']; ?>" class="block" download>
            <svg
              width="107"
              height="121"
              viewBox="0 0 107 121"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M8.12493 0H72.3795L106.437 33.9255V113.438C106.437 117.616 103.049 121 98.8748 121H8.12493C3.95031 121 0.5625 117.616 0.5625 113.438V7.56243C0.5625 3.38429 3.95071 0 8.12493 0Z"
                fill="#E2574C"
                class="first"
              />
              <path
                d="M106.328 34.0312H79.9687C75.7941 34.0312 72.4062 30.6434 72.4062 26.4687V0.0755615L106.328 34.0312Z"
                fill="#B53629"
                class="second"
              />
              <path
                d="M78.0708 57.335C79.3377 57.335 79.9578 56.2309 79.9578 55.1609C79.9578 54.0529 79.3111 52.9828 78.0708 52.9828H70.8562C69.4458 52.9828 68.6593 54.1511 68.6593 55.4406V73.1708C68.6593 74.7514 69.5592 75.6286 70.7768 75.6286C71.9869 75.6286 72.8907 74.7514 72.8907 73.1708V68.3044H77.2543C78.608 68.3044 79.2849 67.1964 79.2849 66.0962C79.2849 65.0187 78.608 63.9483 77.2543 63.9483H72.8907V57.335H78.0708ZM53.6855 52.9828H48.4068C46.9737 52.9828 45.9564 53.966 45.9564 55.4253V73.186C45.9564 74.9971 47.4197 75.5644 48.4671 75.5644H54.0067C60.5633 75.5644 64.8928 71.2501 64.8928 64.5911C64.8893 57.5506 60.8133 52.9828 53.6855 52.9828ZM53.939 71.186H50.721V57.3616H53.6214C58.0115 57.3616 59.9208 60.3073 59.9208 64.372C59.9208 68.1761 58.0452 71.186 53.939 71.186ZM34.6016 52.9828H29.3722C27.8937 52.9828 27.0693 53.9582 27.0693 55.4406V73.1708C27.0693 74.7514 28.0146 75.6286 29.285 75.6286C30.5553 75.6286 31.5006 74.7514 31.5006 73.1708V67.9942H34.7789C38.8248 67.9942 42.1637 65.1279 42.1637 60.5186C42.1641 56.0079 38.9425 52.9828 34.6016 52.9828ZM34.5148 63.8352H31.501V57.1461H34.5148C36.3752 57.1461 37.5587 58.598 37.5587 60.4924C37.5548 62.3833 36.3752 63.8352 34.5148 63.8352Z"
                fill="white"
                class="three"
              />
            </svg>
            <p><?php echo $item['nazvanie_fajla']; ?></p>
          </a>
        <?php } ?>

          
        </div>
      </div>
    </section>



<?php 
  get_template_part( 'components/component', 'form');
?>

<?php 
  get_template_part( 'components/component', 'contact');
?>
<?php get_footer(); ?>