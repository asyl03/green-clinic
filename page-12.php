<?php get_header(); ?>

<div class="page__banner--bread">
  <div class="bread-line">
    <div class="container">
      <?php if( function_exists('dimox_breadcrumbs') ) dimox_breadcrumbs(); ?>
    </div>
  </div>
</div>

<?php 
  get_template_part( 'components/component', 'contact');
?>

<?php 
  get_template_part( 'components/component', 'form');
?>

<?php get_footer(); ?>